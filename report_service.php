<?php 
include("header.php");
require('db/connection.php');
  $sql="SELECT * FROM vecservice";
  $result=pg_query($db,$sql);
  $count = pg_num_rows($result);
 ?>
 <!-- link for jquery scripts -->
 <!-- include jquery -->
    <script src="bootstrap/js/jquery.js"></script>
    <script src="bootstrap/js/jqueryui.js"></script>
 <script language="javascript">
  // ajax query for sending data for the search
function showHint(str) {
    if (str.length == 0) { 
       window.location.replace('report_service.php');
    } else {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("txtHint").innerHTML = this.responseText;               
                            }
        };
        xmlhttp.open("GET", "report_service_search.php?data=" + str, true);
        xmlhttp.send();
    }
}

</script>

 <h2 style="text-align: center;"><u>VEHICLES SERVICING REPORT</u></h2>
 <!-- for showing of number or rows returned -->
 <div class="row">
        <div class="col-md-2">
          <u><h5 style="font-weight: bold;"><?php echo " Results found: ".$count; ?></h5></u>
        </div>
      </div>
 <div class="row">
    <!-- for search  -->
    <div class="col-md-10">
      <form action="#">
           <div class="form-group input-group">
              <input type="text" class="form-control" name="data" placeholder="searh here &hellip;" autocomplete="off" onkeyup="showHint(this.value)" autofocus="autofocus">
               <span class="input-group-btn"><button class="btn btn-default" disabled><i class="fa fa-search"></i></button></span>
            </div>
           </form>
    </div>
      <div class="col-md-2">
        <a href="javascript:Clickheretoprint()" class="btn btn-primary">PRINT</a>
        </div>
      </div>
         <div class="content" id="content">
          <div class="col-md-12">
            <h3 style="text-align: center;"><u>Servicing Report Details</u></h3>
        </div>
      <table class="table table-responsive table-condensed table-striped table-hover table-bordered" style="text-align: center;">
                <thead style="color: black;" class="main-color-bg"> 
                        <tr>  
                          <th style="border-bottom:1px solid #999999">REG NUMBER</th>
                          <th style="border-bottom:1px solid #999999">AMOUNT</th>
                          <th style="border-bottom:1px solid #999999">INVOICE</th>
                          <th style="border-bottom:1px solid #999999">DEALERSHIP</th>
                          <th style="border-bottom:1px solid #999999">SERVICING DATE</th>
                          <th style="border-bottom:1px solid #999999">NEXT MILAGE</th>
                          <th style="border-bottom:1px solid #999999">NEXT SERVICING</th>
                        </tr>  
                </thead> 
                <tbody id="txtHint">
               <?php
                while($row=pg_fetch_array($result)){  //while look to fetch the result and store in a array $row.
                    $vecsrvinvoice = trim(strtoupper($row['vecsrvinvoice']));
                    $vecsrvdeal = trim(strtoupper($row['vecsrvdeal']));
                    $vecsrvdate = trim(strtoupper($row['vecsrvdate']));
                    $vecsrvnxtdate = trim(strtoupper($row['vecsrvnxtdate']));
                    $vecsrvnote = trim(strtoupper($row['vecsrvnote']));
                    $vecsrvmilage = trim(strtoupper($row['vecsrvmilage']));
                    $vecsrvamnt = trim(strtoupper($row['vecsrvamount']));
                    $vecregnum=trim(strtoupper($row['vecregnum'])); 
                    ?> 
                     <tr> 
                        <td style="border-bottom:1px solid #999999"><?php echo $vecregnum;?></td>
                        <td style="border-bottom:1px solid #999999"><?php echo $vecsrvamnt;?></td>
                        <td style="border-bottom:1px solid #999999"><?php echo $vecsrvinvoice;?></td>
                        <td style="border-bottom:1px solid #999999"><?php echo $vecsrvdeal;?></td>
                        <td style="border-bottom:1px solid #999999"><?php echo $vecsrvdate;?></td>
                        <td style="border-bottom:1px solid #999999"><?php echo $vecsrvmilage;?></td>
                        <td style="border-bottom:1px solid #999999"><?php echo $vecsrvnxtdate;?></td>
                      </tr>  
                    <?php } ?> 
                  </tbody>
            </table>
          </div>
 <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
     <script src="cdn.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="bootstrap/js/bootstrap.min.js"></script>

  </body>
</html>

