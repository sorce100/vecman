<?php
include("header.php");
require("db/connection.php");
$inputdata = strtoupper($_POST['data']);

 $sql="SELECT * FROM vecinsurance WHERE CONCAT(insnum,insduedate,snum,inscomname,insnetpremium,vecregnum,inscomname) LIKE '%".$inputdata."%'";
$result=pg_query($db,$sql);

?>
<div class="row">
		<!-- for add button and search button -->
		<div class="col-md-2">
			<button data-toggle="modal" data-target="#myModal" class="btn btn-danger"><span class="glyphicon glyphicon-plus"></span> ADD NEW</button>
		</div>
		<!-- for search  -->
		<div class="col-md-10">
		  <form action="vecInsurancesearch.php" method="POST">
			<div class="form-group input-group">
              <input type="text" class="form-control" placeholder="searh here &hellip;" name="data">
               <span class="input-group-btn"><button class="btn btn-default" type="button"><i class="fa fa-search"></i></button></span>
            </div>
           </form>
		</div>
	</div>

	<!-- div for content -->
	<div class="row">
		<div class="col-lg-12">
          <div class="panel panel-default">
              <div class="panel-heading main-color-bg">
                <h3 class="panel-title">SEARCH DETAILS</h3>
              </div>
              <div class="panel-body">
                
                <table class="table table-responsive table-condensed table-striped table-hover">
                <thead style="color: black;">    
                        <tr> 
                          <th>REG NUMBER</th>
                          <th>INSURANCE NO</th>
                          <th>S/NO</th>  
                          <th>DUE DATE</th>  
                          <th>NAME OF INSURER</th> 
                        </tr>  
                    </thead> 
               <?php
                while($row=pg_fetch_array($result)){  //while look to fetch the result and store in a array $row.
                      $vecinsid=trim(strtoupper($row['vecinsid'])); 
                      $insnum=trim(strtoupper($row['insnum'])); 
                      $snum=trim(strtoupper($row['snum']));  
                      $vecid=trim(strtoupper($row['vecid']));
                      $inscomname=trim(strtoupper($row['inscomname'])); 
                      $date=trim(strtoupper($row['insduedate']));  
                      $vecregnum=trim(strtoupper($row['vecregnum']));  
                    ?> 
                    <tr  id="txtHint"> 
                      <td><?php echo $vecregnum;?></td> 
                      <td><?php echo $insnum;?></td>
                      <td><?php echo $snum;?></td>
                      <td><?php echo $date;?></td>
                      <td><?php echo $inscomname;?></td>
                      <td>
                        <a href="vecInsurance_update.php?data=<?php echo $vecinsid;?>" data-toggle="modal" data-target="#updateModal" class="btn-sm btn-success" aria-label="Update">
                          <i class="fa fa-pencil fa-fw" aria-hidden="true"></i>
                        </a>
                      </td>
                      <td>
                        <a href="vecInsurance_del.php?data=<?php echo $vecinsid;?>" class="btn-sm btn-danger" aria-label="Delete" onclick="return confirm('Are you sure to delete !'); ">
                          <i class="fa fa-trash-o" aria-hidden="true"></i>
                        </a>
                      </td>
                    </tr>  
                    <?php } ?> 
                    </table> 
              </div>
              </div>              
        </div>
	</div>
 </div>

<!-- modal for saving -->
 <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" id="bg">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">ADD INSURANCE</h4>
      </div>
      <div class="modal-body" id="bg">
      	<form method="POST" action="vecInsurance_save.php"> 
            		<div class="row">
                <div class="col-md-6">
                  <!-- for vehicle name -->
                <div class="form-group">
                    <label for="regnum">VEHICLE REQ:</label>
                    <input type="text" class="form-control" id="regnum" placeholder="Enter Vehicle registration number" name="regnum" required>
                </div>
                <!-- insurance number -->
                <div class="form-group">
                    <label for="insnum">INSURANCE NUMBER:</label>
                    <input type="text" class="form-control" id="insnum" placeholder="Enter insurance number" name="insnum">
                </div>
                <!-- for insurance coverage -->
                <div class="form-group">
                    <label for="inscov">INSURANCE COVERAGE:</label>
                    <input type="text" class="form-control" id="inscov" placeholder="Enter insurance coverage" name="inscov">
                </div>
                <!-- S/NUMBER  -->
                <div class="form-group">
                    <label for="inssnum">S/NO:</label>
                    <input type="text" class="form-control" id="inssnum" placeholder="Enter insurance number" name="snum">
                </div>
                
                </div>  
                <div class="col-md-6">
                  <!-- insurance company name -->
                <div class="form-group">
                    <label for="inscomname">INSURANCE COMPANY:</label>
                    <input type="text" class="form-control" id="inscomname" placeholder="Enter insurance number" name="inscomname">
                </div>
                <!-- insurance net premium -->
                <div class="form-group">
                    <label for="insnetpre">NET PREMIUM:</label>
                    <input type="text" class="form-control" id="insnetpre" placeholder="Enter insurance premium" name="insnetpre">
                </div>
                <!-- for insurance policy -->
                <div class="form-group">
                    <label for="inspolicy">POLICY NUMBER:</label>
                    <input type="text" class="form-control" id="inspolicy" placeholder="Enter insurance policy number" name="inspolicy">
                </div>
            		
            		<!-- insurance due dat -->
            		<div class="form-group">
                		<label for="insduedate">DUE DATE:</label>
                		<input type="date" class="form-control" id="insduedate" placeholder="Enter insurance cost in Cedis" name="insduedate" requierd>
            		</div>
            <!-- sending logedin userid -->
            <input name="user_id" value="<?php echo $_SESSION['loginid'] ?>" hidden>
              <div class="well modal-footer" id="bg">
            	<input type="submit" class="btn btn-md btn-danger" name="submit" value="ADD INSURANCE" />
       		 </div>
           </div>
           </div>
        </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- for teh updat modal -->
<div class="modal fade" id="updateModal" tabindex="-1" role="dialog" aria-labelledby="updateModalLabel" aria-hidden="true">
  <div class="modal-dialog ">
    <div class="modal-content">
    </div>
  </div>
</div>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
     <script src="cdn.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="bootstrap/js/bootstrap.min.js"></script>

  </body>
</html>
