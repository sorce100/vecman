<?php 
include("header.php");
require('db/connection.php');
  $sql="SELECT * FROM driverlicence ORDER BY date_done DESC";
  $result=pg_query($db,$sql) or die(pg_last_error());
  $row=pg_fetch_array($results);
 ?>
<script>
function showHint(str) {
    if (str.length == 0) { 
        document.getElementById("txtHint").innerHTML = "";
        return;
    } else {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("txtHint").innerHTML = this.responseText;
                  
                            }
        };
        xmlhttp.open("GET", "vecRegCheck.php?data=" + str, true);
        xmlhttp.send();
    }
}
// function for disabling and enabling the button

  function myFunction() {
    window.location.reload();
                      }

</script>
 
	<div class="row">
    <!-- for add button and search button -->
    <div class="col-md-2">
      <button data-toggle="modal" data-target="#myModal" class="btn btn-danger"><span class="glyphicon glyphicon-plus"></span> ADD LICENCE</button>
    </div>
    <!-- for search  -->
    <div class="col-md-10">
      <form action="vecDriverslinsearch.php" method="POST">
      <div class="form-group input-group">
              <input type="text" class="form-control" placeholder="searh here &hellip;" name="data">
               <span class="input-group-btn"><button class="btn btn-default" type="button"><i class="fa fa-search"></i></button></span>
            </div>
           </form>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-12">
          <div class="panel panel-default">
              <div class="panel-heading main-color-bg">
                <h3 class="panel-title">DRIVERS LICENCE DETAILS</h3>
              </div>
              <div class="panel-body">
                
                <table class="table table-responsive table-condensed table-striped table-hover">
                <thead style="color: black;">    
                        <tr> 
                          <th>FIRST NAME</th>
                          <th>LAST NAME</th>
                          <th>DOB</th>
                          <th>NATIONALITY</th>
                          <th>CENTER</th>
                          <th>EXPIRY DATE</th> 
                        </tr>  
                    </thead> 
               <?php
                while($row=pg_fetch_array($result)){  //while look to fetch the result and store in a array $row.
                      $driverlinid=trim(strtoupper($row['driverlinid']));
                      $fname=trim(strtoupper($row['driverfirstname']));
                      $lname=trim(strtoupper($row['driverlastname'])); 
                      $dob=trim(strtoupper($row['driverdob']));  
                      $nationality=trim(strtoupper($row['drivernationality'])); 
                      $processcen=trim(strtoupper($row['driverprocesscen']));  
                      $expiry=trim(strtoupper($row['driverlinexp']));  
                      $certdate=trim(strtoupper($row['drivercertdate']));  
                    ?> 
                    <tr> 
                      <td><?php echo $fname;?></td>
                      <td><?php echo $lname;?></td>
                      <td><?php echo $dob;?></td>
                      <td><?php echo $nationality;?></td>
                      <td><?php echo $processcen;?></td> 
                      <td><?php echo $expiry;?></td>
                      <td>
                        <a href="vecDriversLin_Update.php?data=<?php echo $driverlinid;?>" data-toggle="modal" data-target="#updateModal" class="btn-sm btn-success" aria-label="Update">
                          <i class="fa fa-pencil fa-fw" aria-hidden="true"></i>
                        </a>
                      </td>
                      <td>
                        <a href="vecDriversLin_del.php?data=<?php echo $driverlinid;?>" class="btn-sm btn-danger" aria-label="Delete" onclick="return confirm('Are you sure to delete !'); ">
                          <i class="fa fa-trash-o" aria-hidden="true"></i>
                        </a>
                      </td>
                    </tr>  
                    <?php } ?> 
                    </table> 
              </div>
              </div>              
        </div>
  </div>	
	

	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog ">
    <div class="modal-content">
      <div class="modal-header" id="bg">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" onClick="myFunction()"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">ADD DRIVER LICENCE:</h4>
      </div>
      <div class="modal-body" id="bg">
      <form method="POST" action="vecDriversLin_save.php"> 
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                <label for="driverfname">FIRST NAME:</label>
                <input type="text" class="form-control" id="driverfname" placeholder="Enter firstname &hellip;" name="driverfname" required>
            </div>
            <div class="form-group">
                <label for="driverlname ">LAST NAME:</label>
                <input type="text" class="form-control" id="driverlname" placeholder="Enter lastname  &hellip;" name="driverlname" required>
            </div>
            <!-- for drivers date of birth -->
            <div class="form-group">
                <label for="driverdob">DATE OF BIRTH:</label>
                <input type="text" class="form-control tcal" id="driverdob" placeholder="Enter Licence holder DOB &hellip;" name="driverdob">
            </div>
            <!-- for processing center -->
             <div class="form-group">
                <label for="driverprocen">PROCESSING CENTER:</label>
                <input type="text" class="form-control" id="driverprocen" placeholder="Enter Licence processing center &hellip;" name="driverprocen">
            </div>
            <!-- for nationality -->
             <div class="form-group">
                <label for="drivernation">NATIONALITY:</label>
                <input type="text" class="form-control" id="drivernation" placeholder="Enter Licence holder nationality &hellip;" name="drivernation">
            </div>
            <!-- for class of licence -->
            <div class="form-group">
                <label for="driverlinclass">LICENCE CLASS:</label>
                <input type="text" class="form-control" id="driverlinclass" placeholder="Enter Licence holder nationality &hellip;" name="driverlinclass">
            </div>
           
              </div>
              <div class="col-md-6">
                 <!-- for date of issue -->
            <div class="form-group">
                <label for="driverlindteissue">DATE OF ISSUE:</label>
                <input type="text" class="form-control tcal" id="driverlindteissue" placeholder="Enter Licence date of issue &hellip;" name="driverlindteissue">
            </div>
            <!-- for expiry date -->
            <div class="form-group">
                <label for="driverlinexp">EXPIRY DATE:</label>
                <input type="text" class="form-control tcal" id="driverlinexp" placeholder="Enter Licence date of expiry &hellip;" name="driverlinexp">
            </div>
            <!-- for date of first licence -->
            <div class="form-group">
                <label for="driverfrstlin">DATE OF FIRST LICENCE:</label>
                <input type="text" class="form-control tcal" id="driverfrstlin" placeholder="Enter date of first licence &hellip;" name="driverfrstlin">
            </div>
            <!-- for certificate date -->
             <div class="form-group">
                <label for="drivercertdte">CERTIFICATE DATE:</label>
                <input type="text" class="form-control tcal" id="drivercertdte" placeholder="Enter date of certificate date &hellip;" name="drivercertdte">
            </div>
            <!-- for certificate of competence -->
            <div class="form-group">
                <label for="drivercertcom">CERTIFICATE OF COMPETENCE:</label>
                <input type="text" class="form-control" id="drivercertcom" placeholder="Enter certificate of competences &hellip;" name="drivercertcom">
            </div>
            <input name="user_id" value="<?php echo $_SESSION['loginid'] ?>" hidden>
            <div class="well modal-footer" id="bg">
            <input type="submit" class="btn btn-danger" name="submit" value="ADD LICENCE" />
              </div>
            </div>
            
        </div>
        </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- for teh updat modal -->
<div class="modal fade" id="updateModal" tabindex="-1" role="dialog" aria-labelledby="updateModalLabel" aria-hidden="true">
  <div class="modal-dialog ">
    <div class="modal-content">
    </div>
  </div>
</div>
</div>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
     <script src="cdn.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="bootstrap/js/bootstrap.min.js"></script>

  </body>
</html>
