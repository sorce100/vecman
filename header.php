<!-- PLEASE DO NOT JUDGE VARIABLE NAMING....LOL -->
<?php 
include("check.php");  
 ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- image at the top of the page -->
    <link rel="shortcut icon" href="images/logo.jpg"/>
    <title>VEHICLE MANAGEMENT APPLICATION</title>
    <!-- Bootstrap -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="bootstrap/css/style.css">
    <!-- include jquery -->
    <script src="bootstrap/js/jquery.js"></script>
    <script src="bootstrap/js/jqueryui.js"></script>
    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <link rel="stylesheet" type="text/css" href="tcal.css" />
    <script type="text/javascript" src="tcal.js"></script>

    <script>
/* Visit http://www.yaldex.com/ for full source code
and get more free JavaScript, CSS and DHTML scripts! */
<!-- Begin
var timerID = null;
var timerRunning = false;
function stopclock (){
if(timerRunning)
clearTimeout(timerID);
timerRunning = false;
}
function showtime () {
var now = new Date();
var hours = now.getHours();
var minutes = now.getMinutes();
var seconds = now.getSeconds()
var timeValue = "" + ((hours >12) ? hours -12 :hours)
if (timeValue == "0") timeValue = 12;
timeValue += ((minutes < 10) ? ":0" : ":") + minutes
timeValue += ((seconds < 10) ? ":0" : ":") + seconds
timeValue += (hours >= 12) ? " P.M." : " A.M."
document.clock.face.value = timeValue;
timerID = setTimeout("showtime()",1000);
timerRunning = true;
}
function startclock() {
stopclock();
showtime();
}
window.onload=startclock;
// End -->

/*function for printing of the page*/
function Clickheretoprint()
{ 
  var disp_setting="toolbar=yes,location=no,directories=yes,menubar=yes,"; 
      disp_setting+="scrollbars=yes,width=700, height=400, left=100, top=25"; 
  var content_vlue = document.getElementById("content").innerHTML; 
  
  var docprint=window.open("","",disp_setting); 
   docprint.document.open(); 
   docprint.document.write('</head><body onLoad="self.print()" style="width: 700px; font-size:11px; font-family:arial; font-weight:normal;">');
   docprint.document.write('<div><img src="images/printlogo.jpg" height="120" width="120" style="position:relative;left:40%;">');
   docprint.document.write('<h2 style="text-align:center;"><u><b>MINISTRY OF LANDS AND NATURAL RESOURCES</b></u></h2');
   docprint.document.write('<br />'); 
   docprint.document.write('<h3 style="text-align:center;"><u><b>VEHICLE MANAGEMENT APPLICATION</b></u></h3');
   docprint.document.write(content_vlue); 
   docprint.document.close(); 
   docprint.focus(); 
}
</script>
  </head>
  <body>
    <nav class="navbar navbar-default">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <!-- <a class="navbar-brand" href="#"> -->
            <form name="clock">
      <font color="white"></font>&nbsp;<input style="width:150px;height: 33px;" type="submit" class="trans" name="face" value="">
      </form>
          <!-- </a> -->
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="#">Welcome, <?php echo $displayname; ?></a></li>
            <li class="dropdown">
              <a href="" class="dropdown-toggle btn cog-back btn-sm " data-toggle="dropdown"><span class="glyphicon glyphicon-cog"></span></a>
                <ul class="dropdown-menu btn">
                  <li> <a href="logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a></li>
                  <li class="divider"></li>
                 <li><a class="dropdown-item" data-toggle="modal" data-target="#passchngModal"><i class="fa fa-fw fa-gear"></i> Change Password</a></li>
                </ul>
            </li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
    <header id="header">
      <div class="container-fluid">
        <div class="row">
          
          <div class="col-md-12">
            <div class="row">
              <div class="col-md-2">
                 <img src="images/logo.jpg" alt="logo" style="height: 40%; width: 40%;">
              </div>
              <div class="col-md-10">
                <h1 id="click"> VEHICLE MANAGEMENT APPLICATION</h1>
              </div>
            </div>
          </div>
          </div>
        </div>
      </div>
    </header>
    <section id="main">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-3">
            <div class="list-group">
              <a href="dashboard.php" class="list-group-item"><span class="glyphicon glyphicon-plane" aria-hidden="true"></span> Dashboard </a>
              <a href="userscomplaint.php" class="list-group-item"><span class="glyphicon glyphicon-plane" aria-hidden="true"></span> Complaints </a>
              <!-- for Vehicles -->
              <a href="#" onclick="javascript:;" class="list-group-item " data-toggle="collapse" data-target="#vecdrop"><span class="glyphicon glyphicon-cog" aria-hidden="true"></span> Vehicles</a>
                <ul id="vecdrop" class="collapse">
                  <li>
                   <a href="vecDetails.php">Vehicle Details</a>
                  </li>
                </ul>
                <!-- for transactions -->
              <a href="#" onclick="javascript:;" class="list-group-item dropmenu" data-toggle="collapse" data-target="#transdrop"><span class="glyphicon glyphicon-th-list" aria-hidden="true"></span> Transactions </a>
              <ul id="transdrop" class="collapse">
                  <li>
                   <a href="vecInsurance.php">Insurance</a>
                  </li>
                  <li>
                   <a href="vecRoadworthy.php">Road Worthy</a>
                  </li>
                  <li>
                    <a href="VecService.php">Servicing</a>
                  </li>
                  <li>
                    <a href="vecTransfer.php">Vehicle Allocation</a>
                  </li>
                </ul>
              <a href="vecDriversLin.php" class="list-group-item"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Drivers Licence </a>

               <?php 
                if ($rank === "ADMIN") {
                  echo '
              <a href="#" onclick="javascript:;" class="list-group-item dropmenu" data-toggle="collapse" data-target="#reportdrop"><span class="glyphicon glyphicon-th-list" aria-hidden="true"></span> Report </a>
              <ul id="reportdrop" class="collapse">
                  <li>
                   <a href="report_complaint.php">User Complaints</a>
                  </li>
                  <li>
                   <a href="report_vehicle.php">Vehicle Report</a>
                  </li>
                  <li>
                   <a href="report_insurance.php">Insurance Report</a>
                  </li>
                  <li>
                    <a href="report_service.php">Servicing Report</a>
                  </li>
                  <li>
                    <a href="report_transfer.php">Allocation Report</a>
                  </li>
                  <li>
                    <a href="report_roadworthy.php">Roadworthy Report</a>
                  </li>
                </ul>
                   <a href="users.php" class="list-group-item"><span class="glyphicon glyphicon-print" aria-hidden="true"></span> Users </a>';
                      }
                    ?>
            </div>
          </div>
          <div class="col-md-9">          
         <!-- for change password modal -->
   <div class="modal fade" id="passchngModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog ">
    <div class="modal-content">
      <div class="modal-header" id="bg">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">CHANGE PASSWORD</h4>
      </div>
      <div class="modal-body" id="bg">
      <form method="POST" action="change_pass.php"> 
            <div class="form-group">
                <label for="oldpass">OLD PASSWORD</label>
                <input type="password" class="form-control" id="oldpass" placeholder="Enter old password &hellip;" name="oldpass" required>
            </div>
            <div class="form-group">
                <label for="newpass ">NEW PASSWORD</label>
                <input type="password" class="form-control" id="newpass" placeholder="Enter new password  &hellip;" name="newpass" required>
            </div>
            <div class="form-group">
                <label for="renewpass">RETYPE NEW PASSWORD</label>
                <input type="password" class="form-control" id="renewpass" placeholder="retype new password  &hellip;" name="renewpass">
            </div>
            <div class="well modal-footer" id="bg">
            <input type="submit" class="btn btn-danger" name="submit" value="CHANGE PASSWORD" />
        </div>
        </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->