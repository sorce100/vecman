<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- image at the top of the page -->
    <link rel="shortcut icon" href="images/logo.jpg"/>
    <title>VEHICLE MANAGEMENT APPLICATION</title>
    <!-- Bootstrap -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="bootstrap/css/style.css">
    <!-- include jquery -->
    <script src="bootstrap/js/jquery.js"></script>
    <script src="bootstrap/js/jqueryui.js"></script>
    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  </head>
<script>
function showHint(str) {
    if (str.length == 0) { 
        document.getElementById("txtHint").innerHTML = "";
        return;
    } else {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("txtHint").innerHTML = this.responseText;               
                            }
        };
        xmlhttp.open("GET", "generalsearch.php?data=" + str, true);
        xmlhttp.send();
    }
}
// function for disabling and enabling the button
</script>
  <body>
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-4"></div>
          <div class="col-md-4 logo">
            <img src="images/logo.jpg" alt="logo" data-toggle="modal" data-target="#myModal" >
          </div>
          <div class="col-md-4"></div>
        </div>
        <div class="row toptitle">
          <div class="col-md-12">
            <u><h1>MINISTRY OF LANDS AND NATURAL RESOURCES</h1></u>
            <u><h2>VEHICLE MANAGEMENT APPLICATION</h2></u>
          </div>
        </div>
        <br>
        <div class="row">
          <div class="col-md-3"></div>
          <div class="col-md-6">
           <form action="#">
            <div class="input-group-lg input-group">
              <input type="text" class="form-control" placeholder="Enter Vehicle registration number e.g GV-123-10" name="data" autocomplete="off" onkeyup="showHint(this.value)" autofocus="autofocus">
               <span class="input-group-btn"><button class="btn btn-danger" type="button"><i class="fa fa-search"></i></button></span>
            </div>
           </form>
          </div>
          <div class="col-md-3"></div>
        </div>
      <br>
      <!-- for displaying contents of the search -->
      <div>
         <div class="row">
          <div class="col-md-2"></div>
           <div class="col-md-8">
             <span id="txtHint"></span>
           </div>
          <div class="col-md-2"></div>
         </div>
        
      </div>
      <!-- for the footer -->
      <br>
      <br>
      <div class="row">
        <div class="col-md-12" style="text-align: center;">
          <footer>
            <small ><b>&copy; Copyright <?php echo date("Y");?>, Ministry Of Lands And Natural Resources</b></small>
          </footer> 
        </div>
      </div>
      <!-- end of container fluid -->
      </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
     <script src="cdn.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
  </body>
</html> 
 <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" id="bg">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title">ADMINISTRATOR LOGIN</h3>
      </div>
      <div class="modal-body" id="bg">
        <form role="form" action="login.php" method="post" class="login-form">
                <h1>LOGIN</h1>
             <div class="input-group">
                  <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                  <input id="username" type="text" class="form-control input-lg" name="username" placeholder="Username" required="" autofocus="autofocus" autocomplete="off">
                </div>
                <br>
                <div class="input-group">
                  <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                  <input id="password" type="password" class="form-control input-lg" name="password" placeholder="Password" required="">
                </div>
                <br>
              <div>
                <div>
                  <button type="submit" class="btn btn-lg btn-danger btn-block waves-effect waves-light">LOGIN</button> 
                </div>
              </div>
            </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->