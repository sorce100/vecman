<?php 
include("header.php");
require('db/connection.php');
	$inputdata = strtoupper($_POST['data']);
 $sql="SELECT * FROM vechicletransfer WHERE CONCAT(vectransfrom,vectransto,vectransdate,vectranscon,vectranstodiv,vectransfromdiv,vecregnum) LIKE '%".$inputdata."%'";
$result=pg_query($db,$sql);
 
 ?>
	<div class="row">
		<!-- for add button and search button -->
		<div class="col-md-2">
			<button data-toggle="modal" data-target="#myModal" class="btn btn-danger"><span class="glyphicon glyphicon-plus"></span> ADD NEW</button>
		</div>
		<!-- for search  -->
		<div class="col-md-10">
		  <form action="vecTransfersearch.php" method="POST">
			<div class="form-group input-group">
              <input type="text" class="form-control" placeholder="searh here &hellip;" name="data">
               <span class="input-group-btn"><button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button></span>
            </div>
           </form>
		</div>
	</div>

	<!-- div for content -->
	<div class="row">
		<div class="col-lg-12">
          <div class="panel panel-default">
              <div class="panel-heading main-color-bg">
                <h3 class="panel-title">VEHICLE ALLOCATION DETAILS</h3>
              </div>
              <div class="panel-body">
                
                <table class="table table-responsive table-condensed table-striped table-hover">
                <thead style="color: black;">    
                        <tr> 
                          <th>REG NUMBER</th>
                          <th>DIVISION FROM</th>
                          <th>PREVIOUS USER</th>
                          <th>DIVISION TO</th>
                          <th>NEW USER</th> 
                          <th>DATE</th>
                        </tr>  
                    </thead> 
               <?php
                while($row=pg_fetch_array($result)){  //while look to fetch the result and store in a array $row.
                      $vectransid=trim(strtoupper($row['vectransid']));
                      $vecid=trim(strtoupper($row['vecid'])); 
                      $olduser=trim(strtoupper($row['vectransfrom']));  
                      $newuser=trim(strtoupper($row['vectransto'])); 
                      $date=trim(strtoupper($row['vectransdate']));  
                      $transto=trim(strtoupper($row['vectranstodiv']));  
                      $transfrom=trim(strtoupper($row['vectransfromdiv']));  
                      $vecregnum=trim(strtoupper($row['vecregnum']));  
                    ?> 
                    <tr> 
                      <td><?php echo $vecregnum;?></td>
                      <td><?php echo $transfrom;?></td>
                      <td><?php echo $olduser;?></td>
                      <td><?php echo $transto;?></td>
                      <td><?php echo $newuser;?></td> 
                      <td><?php echo $date;?></td>
                      <td>
                        <a href="vecTransfer_update.php?data=<?php echo $vectransid;?>" data-toggle="modal" data-target="#updateModal" class="btn-sm btn-success" aria-label="Update">
                          <i class="fa fa-pencil fa-fw" aria-hidden="true"></i>
                        </a>
                      </td>
                      <td>
                        <a href="vecTransfer_del.php?data=<?php echo $vectransid;?>" class="btn-sm btn-danger" aria-label="Delete" onclick="return confirm('Are you sure to delete !'); ">
                          <i class="fa fa-trash-o" aria-hidden="true"></i>
                        </a>
                      </td>
                    </tr>  
                    <?php } ?> 
                    </table> 
              </div>
              </div>              
        </div>
	</div>
 </div>
<!-- modal for saving -->
 <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" id="bg">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">ADD VEHICLE ALLOCATION</h4>
      </div>
      <div class="modal-body" id="bg">
      	<form method="POST" action="vecTransfer_save.php"> 
            		<!-- for vehicle name -->
            		<div class="form-group">
                		<label for="vecreq">VEHICLE REQ:</label>
                		<input type="text" class="form-control" id="vecreq" placeholder="Enter Vehicle registration number" name="vecreq" required>
            		</div>
            		<!-- for former user -->
            		<div class="form-group">
                		<div class="row">
                      <div class="col-md-8">
                        <label for="vectransfrom">ASSIGNED FROM:</label>
                         <input type="text" class="form-control" id="vectransfrom" placeholder="Previous user of vehcile" name="vectransfrom" required>
                      </div>
                      <div class="col-md-4">
                        <label for="vectransfromdiv">DIVISION:</label>
                        <select name="vectransfromdiv" class="form-control">
                          <option>...........................</option>
                          <option value="pvlmd">PVLMD</option>
                          <option value="lvd">LVD</option>
                          <option value="smd">SMD</option>
                          <option value="lrd">LRD</option>
                           <option value="cooperate">COOPERATE</option>
                         </select>
                      </div>
                    </div>
            		</div>
            		<!-- for new user -->
            		<div class="form-group">
                    <div class="row">
                      <div class="col-md-8">
                        <label for="vectransto">ASSIGNED TO:</label>
                         <input type="text" class="form-control" id="vectransto" placeholder="New user of vehcile" name="vectransto" required>
                      </div>
                      <div class="col-md-4">
                        <label for="vectranstodiv">DIVISION:</label>
                        <select name="vectranstodiv" class="form-control">
                          <option>...........................</option>
                          <option value="pvlmd">PVLMD</option>
                          <option value="lvd">LVD</option>
                          <option value="smd">SMD</option>
                          <option value="lrd">LRD</option>
                           <option value="cooperate">COOPERATE</option>
                         </select>
                      </div>
                    </div>
                </div>
            		<!-- date -->
            		<div class="row">
                <div class="col-md-8">
                  <div class="form-group">
                    <label for="vectransdate">ASSIGNED DATE:</label>
                    <input type="date" class="form-control" id="vectransdate" placeholder="Enter Vehicle id" name="vectransdate" required>
                </div>
                </div>

                <div class="col-md-4">
                  <!-- vehicle condition -->
                <div class="form-group">
                    <label for="vectranscon">VEHICLE CONDITION:</label>
                    <select name="vectranscon" class="form-control">
                      <option>...................................</option>
                        <option value="used">Used</option>
                               <option value="new">New</option>
                           </select>
                </div>
                </div>  
                </div>		
            <!-- sending logedin userid -->
            <input name="user_id" value="<?php echo $_SESSION['loginid'] ?>" hidden>
              <div class="well modal-footer" id="bg">
            	<input type="submit" class="btn btn-md btn-danger" name="submit" value="ADD TRANSFER" />
       		 </div>
        </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- for teh updat modal -->
<div class="modal fade" id="updateModal" tabindex="-1" role="dialog" aria-labelledby="updateModalLabel" aria-hidden="true">
  <div class="modal-dialog ">
    <div class="modal-content">
    </div>
  </div>
</div>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
     <script src="cdn.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="bootstrap/js/bootstrap.min.js"></script>

  </body>
</html>
