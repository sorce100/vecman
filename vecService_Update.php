 <?php   
require('db/connection.php');
include("check.php");  
$input=$_GET["data"];
$sql="SELECT * FROM vecservice WHERE vecsrvid='$input'";
  $result=pg_query($db,$sql);
  $row=pg_fetch_array($result);
  if (!$result) {
     header("Location:vecService.php");
  }else{
    $vecsrvid = trim(strtoupper($row['vecsrvid']));
    $vecid = trim(strtoupper($row['vecid']));
    $vecsrvinvoice = trim(strtoupper($row['vecsrvinvoice']));
    $vecsrvdeal = trim(strtoupper($row['vecsrvdeal']));
    $vecsrvdate = trim(strtoupper($row['vecsrvdate']));
    $vecsrvnxtdate = trim(strtoupper($row['vecsrvnxtdate']));
    $vecsrvnote = trim(strtoupper($row['vecsrvnote']));
    $vecsrvmilage = trim(strtoupper($row['vecsrvmilage']));
    $vecsrvamnt = trim(strtoupper($row['vecsrvamount']));
    $vecregnum = trim(strtoupper($row['vecregnum']));
  }
 ?>
 <script>
  function myFunction() {
    window.location.reload();
                      }
</script>
<div class="modal-header" id="bg">
        <button type="button" class="close" data-dismiss="modal" onClick="myFunction()" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">UPDATE VEHICLE SERVICING</h4>
      </div>
<div class="modal-body" id="bg">
       <form method="POST" action="vecService_UpdateScript.php"> 
                <div class="row">
                  <div class="col-md-6">
                    <!-- for vehicle name -->
                <div class="form-group">
                    <label for="reqnum">REG NUMBER:</label>
                    <input type="text" class="form-control" id="reqnum" value="<?php echo $vecregnum;?>" disabled>
                </div>
                 <!-- for amount spent on the servicing -->
                  <label for="foramount">SERVICING AMOUNT:</label>
                  <div class="form-group input-group">
                    <span class="input-group-addon">₵</span>
                    <input type="text" class="form-control" value="<?php echo $vecsrvamnt;?>" name="vecsrvamnt">
                    <span class="input-group-addon">.00</span>
                  </div>
                <!-- for servicing invoice -->
                <div class="form-group">
                    <label for="vecsrvinv">SERVICING INVOICE:</label>
                    <input type="text" class="form-control" id="vecsrvinv" value="<?php echo $vecsrvinvoice;?>" name="vecsrvinv" >
                </div>
                 <!-- for servicing notes -->
                <div class="form-group">
                    <label for="vecsrvnote">SERVICING NOTES:</label>
                    <textarea type="text" class="form-control" id="vecsrvnote" name="vecsrvnote"><?php echo $vecsrvnote;?></textarea>
                </div>
                <!-- button for printing -->
                    <div  id="bg">
                    <a href="vecService_report.php?data=<?php echo $vecregnum;?>" class="btn btn-md btn-default">PRINT<span class="glyphicon glyphicon-print"></span></a>
                 </div>
                  </div>
                  <div class="col-md-6">
                    <!-- for company -->
                <div class="form-group">
                    <label for="vecsrvcom">SERVICING DEALERSHIP:</label>
                    <input type="text" class="form-control" id="vecsrvcom" value="<?php echo $vecsrvdeal;?>" name="vecsrvcom" >
                </div>
                    <!-- for servicing date -->
                <div class="form-group">
                    <label for="vecsrvdate">SERVICING DATE:</label>
                    <input type="text" class="form-control" id="vecsrvdate" value="<?php echo $vecsrvdate;?>" name="vecsrvdate">
                </div>
                 <!-- for previous servicing milage -->
                 <?php 
                 // for retrieving the previous servicing milage
                    $sql="SELECT vecsrvmilage_old FROM vecservice_history WHERE vecsrvid='$input' ORDER BY date_done DESC";
                    $result=pg_query($db,$sql);
                    $row=pg_fetch_array($result);
                    $vecsrvmilage_old = strtoupper($row['vecsrvmilage_old']);
                  ?>
                <div class="form-group">
                    <label for="vecsrvmilage">PREVIOUS SERVICING MILAGE:</label>
                    <input type="text" class="form-control" id="vecsrvmilage" value="<?php echo $vecsrvmilage_old;?>" disabled>
                </div>
                <!-- for next servicing milage -->
                <div class="form-group">
                    <label for="vecsrvmilage">NEXT SERVICING MILAGE:</label>
                    <input type="text" class="form-control" id="vecsrvmilage" value="<?php echo $vecsrvmilage;?>" name="vecsrvmilage">
                </div>
                <!-- for next servicing -->
                <div class="form-group">
                    <label for="vecsrvnxtdate">NEXT SERVICING DATE:</label>
                    <input type="text" class="form-control" id="vecsrvnxtdate" value="<?php echo $vecsrvnxtdate;?>" name="vecsrvnxtdate">
                </div>
               
                   <div class="well modal-footer" id="bg">
              <input type="submit" class="btn btn-md btn-danger" name="submit" value="UPDATE" />
           </div>
                  </div>
                </div>
            <!-- sending logedin userid -->
            <input name="vecsrvid" value="<?php echo $vecsrvid; ?>" hidden>
            <input name="user_id" value="<?php echo $_SESSION['loginid'] ?>" hidden>
            <input name="vecreg" value="<?php echo  $vecregnum ?>" hidden>
             
        </form>
      </div>