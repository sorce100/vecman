<?php
include('check.php');
require('db/connection.php');
$inputid=$_GET["data"];
$sql="SELECT * FROM vecinsurance WHERE vecregnum='$inputid'";
$result=pg_query($db,$sql);
$row=pg_fetch_array($result);

	$insnum=strtoupper($row['insnum']);  
    $insduedate=strtoupper($row['insduedate']); 
    $inscomname=strtoupper($row['inscomname']); 
    $snum=strtoupper($row['snum']);
    $inscoverage=strtoupper($row['inscoverage']); 
    $insnetpremium=strtoupper($row['insnetpremium']); 
    $inspolicy=strtoupper($row['inspolicy']); 
    $vecregnum=strtoupper($row['vecregnum']);  

require('fpdf/fpdf.php');
$pdf = new FPDF('P','mm','A4');
$pdf->AddPage();

// width,ss
$pdf->Image("images/printlogo.jpg",70,10,60,60);
// setting fonts using timees new rman
$pdf->SetFont('Times','B',12);
// line break
$pdf->Ln(60);
// for the page title
$pdf->SetFont('Times','BU',20);
$pdf->Cell(190,10,'MINISTRY OF LANDS AND NATURAL RESOURCES',0,1,'C');
$pdf->Cell(190,10,'VEHICLE MANAGEMENT APPLICATION',0,1,'C');
$pdf->Ln(5);
// for vechicle details
// setting leftmargin
$pdf->SetLeftMargin(30);
$pdf->Cell(60,10,"Insurance details:",0,1);
// for the body
$pdf->SetFont('Times','B',12);
// for reg number
$pdf->Cell(45,10,'REG NUMBER:',0,0);
$pdf->Cell(80,10,$vecregnum,1,1);
$pdf->Ln(5);

// for chasis number
$pdf->Cell(45,10,'INSURANCE NO:',0,0);
$pdf->Cell(80,10,$insnum,1,1);
$pdf->Ln(5);
// for make
$pdf->Cell(45,10,'INSURANCE COV:',0,0);
$pdf->Cell(80,10,$inscoverage,1,1);
$pdf->Ln(5);

// for model
$pdf->Cell(45,10,'CERTIFICATE NO:',0,0);
$pdf->Cell(80,10,$snum,1,1);
$pdf->Ln(5);
// for premium
$pdf->Cell(45,10,'NET PREMIUM:',0,0);
$pdf->Cell(80,10,$insnetpremium,1,1);
$pdf->Ln(5);
// for COLOR
$pdf->Cell(45,10,'COMPANY:',0,0);
$pdf->Cell(80,10,$inscomname,1,1);
$pdf->Ln(5);

// for seating
$pdf->Cell(45,10,'POLICY NUMBER:',0,0);
$pdf->Cell(80,10,$inspolicy,1,1);
$pdf->Ln(5);
// for fuel type
$pdf->Cell(45,10,'DUE DATE:',0,0);
$pdf->Cell(80,10,$insduedate,1,1);
$pdf->Ln(20);

$pdf->Cell(100,10,'.................................................',0,1);
$pdf->Cell(50,10,$displayname,0,1,'C');
$pdf->Output();
?>