<?php
require('db/connection.php');

// get the q parameter from URL
$data = strtoupper($_REQUEST["data"]);
$sql="SELECT V.vecregnum,V.vecfuel,I.insduedate,S.vecsrvmilage,S.vecsrvnxtdate FROM vehicle V
		LEFT JOIN vecinsurance I ON V.vecregnum = I.vecregnum 
		LEFT JOIN vecservice S ON S.vecregnum = V.vecregnum
		WHERE V.vecregnum = '".$data."'";
$result=pg_query($db,$sql);
$row=pg_fetch_array($result);
// data retrieval
$vecregnum = $row['vecregnum'];
$vecfuel = $row['vecfuel'];
$insduedate = $row['insduedate'];
$vecsrvmilage = $row['vecsrvmilage'];
$vecsrvnxtdate = $row['vecsrvnxtdate'];
// Output "no results found" if no hint was found or output correct values 
if (isset($vecregnum)) {

	echo "
		<table border='1' style='width:100%; text-align:center;'>
			<tr>
				<th>REG NUMBER</th>
				<th>FUEL TYPE</th>
				<th>INSURANCE EXPIRY</th>
				<th>NEXT SERVICING MILAGE</th>
				<th>NEXT SERVICING DATE</th>
			</tr>
			<tr style='background-color:#FFFACD;font-weight:50px;'>
				<td>$vecregnum</td>
				<td>$vecfuel</td>
				<td>$insduedate</td>
				<td>$vecsrvmilage</td>
				<td>$vecsrvnxtdate</td>
			</tr>
		</table>		
	";
}

?>

