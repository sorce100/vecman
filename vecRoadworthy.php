<?php 
include("header.php");
require('db/connection.php');
	$sql="SELECT * FROM vecroadworthy ORDER BY date_done DESC";
	$result=pg_query($db,$sql) or die(pg_last_error());
	$row=pg_fetch_array($results);
 ?>

 <script>
function showHint(str) {
    if (str.length == 0) { 
        document.getElementById("txtHint").innerHTML = "";
        return;
    } else {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("txtHint").innerHTML = this.responseText;
                  
                            }
        };
        xmlhttp.open("GET", "vecRegCheck.php?data=" + str, true);
        xmlhttp.send();
    }
}
// function for disabling and enabling the button
 function myFunction() {
    window.location.reload();
                      }

</script>
	<div class="row">
		<!-- for add button and search button -->
		<div class="col-md-2">
			<button data-toggle="modal" data-target="#myModal" class="btn btn-danger"><span class="glyphicon glyphicon-plus"></span> ADD NEW</button>
		</div>
		<!-- for search  -->
		<div class="col-md-10">
		  <form action="vecRoadworthysearch.php" method="POST">
			<div class="form-group input-group">
              <input type="text" class="form-control" placeholder="searh here &hellip;" name="data">
               <span class="input-group-btn"><button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button></span>
            </div>
           </form>
		</div>
	</div>

	<!-- div for content -->
	<div class="row">
		<div class="col-lg-12">
          <div class="panel panel-default">
              <div class="panel-heading main-color-bg">
                <h3 class="panel-title">VEHICLE ROAD WORTHY DETAILS</h3>
              </div>
              <div class="panel-body">
                
                <table class="table table-responsive table-condensed table-striped table-hover">
                <thead style="color: black;">    
                        <tr> 
                          <th>REG NUMBER</th> 
                          <th>RECEIPT NUMBER</th> 
                          <th>STICKER NUMBER</th>
                          <th>NEXT INSPECTION DATE</th>
                        </tr>  
                    </thead> 
               <?php
                while($row=pg_fetch_array($result)){  //while look to fetch the result and store in a array $row.
                      $roadid=strtoupper($row['vecrowoid']); 
                      $vecid=strtoupper($row['vecid']); 
                      $roadrecno=strtoupper($row['roadrecno']); 
                      $roadsticknum=strtoupper($row['roadsticknum']);
                      $roadnxtinpec=strtoupper($row['roadnxtinpec']);  
                      $vecregnum=strtoupper($row['vecregnum']);  
                    ?> 
                    <tr> 
                      <td><?php echo $vecregnum;?></td>
                      <td><?php echo $roadrecno;?></td> 
                      <td><?php echo $roadsticknum;?></td>
                      <td><?php echo $roadnxtinpec;?></td>
                      <td>
                        <a href="vecRoadworthy_update.php?data=<?php echo $roadid;?>" data-toggle="modal" data-target="#updateModal" class="btn-sm btn-success" aria-label="Update">
                          <i class="fa fa-pencil fa-fw" aria-hidden="true"></i>
                        </a>
                      </td>
                      <td>
                        <a href="vecRoadworthy_del.php?data=<?php echo $roadid;?>" class="btn-sm btn-danger" aria-label="Delete" onclick="return confirm('Are you sure to delete !'); ">
                          <i class="fa fa-trash-o" aria-hidden="true"></i>
                        </a>
                      </td>
                    </tr>  
                    <?php } ?> 
                    </table> 
              </div>
              </div>              
        </div>
	</div>
 </div>
<!-- modal for saving -->
 <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" id="bg">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" onClick="myFunction()"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">ADD ROAD WORTHY</h4>
      </div>
      <div class="modal-body" id="bg">
      	<form method="POST" action="vecRoadworthy_save.php"> 
            		<div class="row">
            			<div class="col-md-6">
            				<div class="row">
                    <div class="col-md-12">
                      <!-- for vehicle name -->
                       <div class="form-group">
                           <label for="vecreq">VEHICLE REG:</label>
                           <input type="text" class="form-control" id="regnum" onkeyup="showHint(this.value)" name="vecreq" required autocomplete="off">
                          
                       </div>
                    </div>
                  </div> 
            		<!-- for amount paid -->
            		<label for="roadamt">AMOUNT PAID:</label>
                <div class="form-group input-group">
                  <span class="input-group-addon">₵</span>
                  <input type="text" class="form-control" name="roadamt">
                  <span class="input-group-addon">.00</span>
                </div>
               		<!--receipt number  -->
               		<div class="form-group">
                		<label for="roadrecno">RECEIPT NUMBER:</label>
                		<input type="text" class="form-control" id="roadrecno" placeholder="Enter receipt number" name="roadrecno" >
            		  </div>
            			</div>
            			<div class="col-md-6">
            		<!-- date of inpection -->
            		<div class="form-group">
                		<label for="roadinspsdte">DATE OF INSPECTION:</label>
                		<input type="text" class="form-control tcal" id="roadinspsdte" name="roadinspsdte" autocomplete="off">
            		</div>
            		<!-- sticker number -->
					<div class="form-group">
                		<label for="roadsticknum">STICKER NUMBER:</label>
                		<input type="text" class="form-control" id="roadsticknum" placeholder="Enter sticker number" name="roadsticknum" >
            		</div>
            		<!-- NEXT INSPECTION DATE -->
					<div class="form-group">
                		<label for="roadnxtinpec">NEXT INPECTION DATE:</label>
                		<input type="text" class="form-control tcal" id="roadnxtinpec" name="roadnxtinpec" autocomplete="off">
            		</div>
            	</div>
            		</div>
            <!-- sending logedin userid -->
            <input name="user_id" value="<?php echo $_SESSION['loginid'] ?>" hidden>
              <div class="well modal-footer" id="bg">
            	<span id="txtHint"></span>  
       		 </div>
        </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- for teh updat modal -->
<div class="modal fade" id="updateModal" tabindex="-1" role="dialog" aria-labelledby="updateModalLabel" aria-hidden="true">
  <div class="modal-dialog ">
    <div class="modal-content">
    </div>
  </div>
</div>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
     <script src="cdn.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="bootstrap/js/bootstrap.min.js"></script>

  </body>
</html>
