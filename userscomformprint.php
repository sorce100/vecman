<?php
include('check.php');
require('db/connection.php');
$inputid=$_GET["data"];
$sql="SELECT * FROM userscomplaint WHERE uscomid='$inputid'";
$result=pg_query($db,$sql);
$row=pg_fetch_array($result);

	$comname=strtoupper($row['comname']); 
    $vecreg=strtoupper($row['vecreg']);  
    $comdetails=strtoupper($row['comdetails']); 
    $comdate=strtoupper($row['comdate']); 
    $comphnum=strtoupper($row['comphnum']);
    $comremarks=strtoupper($row['comremarks']);

require('fpdf/fpdf.php');
$pdf = new FPDF('P','mm','A4');
$pdf->AddPage();
// width,ss
$pdf->Image("images/printlogo.jpg",70,10,60,60);
// setting fonts using timees new rman
$pdf->SetFont('Times','B',12);
// line break
$pdf->Ln(60);
// for the page title
$pdf->SetFont('Times','BU',20);
$pdf->Cell(190,10,'MINISTRY OF LANDS AND NATURAL RESOURCES',0,1,'C');
$pdf->Cell(190,10,'VEHICLE USER COMPLAINT FORM',0,1,'C');
$pdf->Ln(2);
// for vechicle details
// setting leftmargin
// $pdf->SetLeftMargin(30);
// $pdf->Cell(60,10,"RoadWorthy details:",0,1);
$pdf->SetLeftMargin(25);
// for the body
$pdf->SetFont('Times','B',14);
// for name
$pdf->Cell(20,10,'FULL NAME:',0,1);

$pdf->SetFont('Times','',12);
$pdf->Cell(160,10,$comname,1,1);
$pdf->Ln(1);

// for registration number
$pdf->SetFont('Times','B',14);
$pdf->Cell(20,10,'REGISTRATION NUMBER:',0,1);
$pdf->SetFont('Times','',12);
$pdf->Cell(160,10,$vecreg,1,1);
$pdf->Ln(1);
// for details
$pdf->SetFont('Times','B',14);
$pdf->Cell(20,10,'PROBLEM:',0,1);
$pdf->SetFont('Times','',12);
$pdf->MultiCell(160,35,$comdetails,1,1);
$pdf->Ln(1);
// for remarks
$pdf->SetFont('Times','B',14);
$pdf->Cell(20,10,'REMARKS:',0,1);
$pdf->SetFont('Times','',12);
$pdf->Cell(160,35,$comremarks,1,1);
$pdf->Ln(1);
// for phone number
$pdf->SetFont('Times','B',14);
$pdf->Cell(20,10,'PHONE NUMBER:',0,1);
$pdf->SetFont('Times','',12);
$pdf->MultiCell(160,10,$comphnum,1,1);
$pdf->Ln(1);
// for date
$pdf->SetFont('Times','B',14);
$pdf->Cell(20,10,'DATE:',0,1);
$pdf->SetFont('Times','',12);
$pdf->Cell(70,10,$comdate,1,0);
$pdf->Ln(2);


$pdf->Output();
?>