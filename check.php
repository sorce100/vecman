<?php
require("auth.php");
include("db/connection.php");
$userid=$_SESSION['loginid'];

$sql = "SELECT * FROM users WHERE userid ='".$userid."'";
$results=pg_query($db,$sql) or die(pg_last_error());
$row=pg_fetch_array($results);

$displayname=$row['fname']." ".$row['lname'];
$_SESSION['displayname'] = $displayname;
$rank = trim(stripslashes($row["rank"]));

$TimeOutMinutes = 60; // This is your TimeOut period in minutes
$LogOff_URL = "index.php"; // If timed out, it will be redirected to this page
$TimeOutSeconds = $TimeOutMinutes * 60; // TimeOut in Seconds
if (isset($_SESSION['SessionStartTime'])) {
    $InactiveTime = time() - $_SESSION['SessionStartTime'];
    if ($InactiveTime >= $TimeOutSeconds) {
        session_destroy();
        header("Location: $LogOff_URL");
    }
}
$_SESSION['SessionStartTime'] = time();

?>  