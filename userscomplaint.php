<?php 
include("header.php");
require('db/connection.php');
// sql query for selecting all complaints
$sql="SELECT * FROM userscomplaint ORDER BY date_done DESC";
  $result=pg_query($db,$sql) or die(pg_last_error());
  $row=pg_fetch_array($results);
?>
<!-- script for sending data for search -->
<script>
// function for disabling and enabling the button
  function myFunction() {
    window.location.reload();
                      }

</script>
<!-- for header -->
<div class="row">
    <!-- for add button and search button -->
    <div class="col-md-2">
      <button data-toggle="modal" data-target="#myModal" class="btn btn-danger"><span class="glyphicon glyphicon-plus"></span> ADD COMPLAINT</button>
    </div>
    <!-- for search  -->
    <div class="col-md-10">
      <form action="user_complaint_search.php" method="POST">
      	<div class="form-group input-group">
          <input type="text" class="form-control" placeholder="searh here &hellip;" name="data">
           <span class="input-group-btn"><button class="btn btn-default" type="button"><i class="fa fa-search"></i></button></span>
        </div>
      </form>
    </div>
  </div>

<!-- for the body -->
<div class="row">
    <div class="col-lg-12">
          <div class="panel panel-default">
              <div class="panel-heading main-color-bg">
                <h3 class="panel-title">COMPLAINTS DETAILS</h3>
              </div>
              <div class="panel-body">
                
                <table class="table table-responsive table-condensed table-striped table-hover">
                <thead style="color: black;text-align: center;">    
                        <tr> 
                          <th>REGISTRATION</th>
                          <th>NAME</th>
                          <th>CATEGORY</th>
                          <th>DETAILS</th>
                          <th>DATE</th>
                           <th>STATUS</th> 
                        </tr>  
                    </thead> 
               <?php
                while($row=pg_fetch_array($result)){  //while look to fetch the result and store in a array $row.
                      $uscomid=trim(strtoupper($row['uscomid']));
                      $comname=trim(strtoupper($row['comname']));
                      $vecreg=trim(strtoupper($row['vecreg'])); 
                      $comcategory=trim(strtoupper($row['comcategory']));  
                      $comdetails=trim(strtoupper($row['comdetails'])); 
                      $comdate=trim(strtoupper($row['comdate']));  
                      $comphnum=trim(strtoupper($row['comphnum']));  
                      $comstatus=trim(strtoupper($row['comstatus']));
                    ?> 
                    <tr> 
                      <td><?php echo $vecreg;?></td>
                      <td><?php echo $comname;?></td>
                      <td><?php echo $comcategory;?></td>
                      <td><?php echo $comdetails;?></td> 
                      <td><?php echo $comdate;?></td>
                      <td><?php echo $comstatus;?></td>
                      <td>
                        <a href="userscomplaint_update.php?data=<?php echo $uscomid;?>" data-toggle="modal" data-target="#updateModal" class="btn-sm btn-success" aria-label="Update">
                          <i class="fa fa-pencil fa-fw" aria-hidden="true"></i>
                        </a>
                      </td>
                      <td>
                        <a href="userscomformprint.php?data=<?php echo $uscomid;?>" class="btn-sm btn-primary" aria-label="Print">
                          <i class="glyphicon glyphicon-print" aria-hidden="true"></i>
                        </a>
                        
                      </td>
                      <td>
                        <a href="userscomplaint_del.php?data=<?php echo $uscomid;?>" class="btn-sm btn-danger" aria-label="Delete" onclick="return confirm('Are you sure to delete !'); ">
                          <i class="fa fa-trash-o" aria-hidden="true"></i>
                        </a>

                      </td>
                    </tr>  
                    <?php } ?> 
                    </table> 
              </div>
              </div>              
        </div>
  </div>	
<!-- modal for complaint form -->
  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" id="bg">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" onClick="myFunction()"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">ADD USER COMPLAINT:</h4>
      </div>
      <div class="modal-body" id="bg">
      <form method="POST" action="userscomplaint_save.php"> 
      	<!-- for complaints name -->
                <div class="form-group">
               	 	<label for="comname">COMPLAINT NAME:</label>
                	<input type="text" class="form-control" id="comname" placeholder="Enter complaints name &hellip;" name="comname" required>
            	</div>
            	<div class="row">
            		<div class="col-md-6">
            	<!-- for vehicle registration number -->
            	 <div class="form-group">
               	 	<label for="vecreg">REGISTRATION NUMBER:</label>
                	<input type="text" class="form-control" id="vecreg" placeholder="Enter vehice registration &hellip;" name="vecreg">
            	</div>
            </div>
            	<div class="col-md-6">
            		<!-- for category -->
            		<div class="form-group">
                		<label for="comcategory">COMPLAINT CATEGORY:</label>
                		<select name="comcategory" class="form-control">
                				<option value="general">General</option>
                               <option value="servicing">Servicing</option>
                               <option value="insurance">Insurance</option>
                               <option value="roadworthy">Road Worthy</option>
                               <option value="allocation">Allocation</option>
                                
                           </select>
            		</div>
            	</div>
            </div>
            		<!-- complaint details -->
            		<div class="form-group">
                		<label for="comdetails" style="color: black;">COMPLAINT DETAILS </label>
                		<textarea class="form-control" rows="4" placeholder="Enter complaint details" name="comdetails"></textarea>
            		</div>
                <!-- for remarks -->
                <div class="form-group">
                    <label for="comremarks" style="color: black;">REMARKS </label>
                    <textarea class="form-control" rows="4" placeholder="Enter complaint remarks" name="comremarks"></textarea>
                </div>
           <!-- date for complaint -->
            	<div class="row">
            		<div class="col-md-6">
            			<div class="form-group">
                			<label for="comdate">DATE OF COMPLAINT:</label>
                			<input type="text" class="form-control tcal" id="comdate" placeholder="Enter Licence date of issue &hellip;" name="comdate">
                  </div>
                  <!-- for the status of complaint -->
                  <div class="form-group">
                      <label for="comstatus">COMPLAINT STATUS:</label>
                              <select name="comstatus" class="form-control">
                                  <option value="pending">PENDING</option>
                                 <option value="resolved">RESOLVED</option>
                             </select>
                  </div>
                    <div  id="bg">
                    <a href="userscomform.php" class="btn btn-md btn-default">PRINT <span class="glyphicon glyphicon-print"></span></a>
                 </div>
            		</div>
            		<!-- for second column -->
            		<div class="col-md-6">
            			<div class="form-group">
                			<label for="comphnum">PHONE NUMBER:</label>
                			<input type="text" class="form-control" id="comphnum" placeholder="Phone number of complaint &hellip;" name="comphnum">
           				</div>
                  <!-- button -->
                  <div class="well modal-footer" id="bg">
              <input type="submit" class="btn btn-danger" name="submit" value="ADD COMPLAINT" />
            </div>
            		</div>
            	</div>
            
            <input name="user_id" value="<?php echo $_SESSION['loginid'] ?>" hidden>
            
        </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- for teh updat modal -->
<div class="modal fade" id="updateModal" tabindex="-1" role="dialog" aria-labelledby="updateModalLabel" aria-hidden="true">
  <div class="modal-dialog ">
    <div class="modal-content">
    </div>
  </div>
</div>
  <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
     <script src="cdn.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="bootstrap/js/bootstrap.min.js"></script>

  </body>
</html>

