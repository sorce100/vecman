<?php 
include("header.php");
require('db/connection.php');
	$sql="SELECT V.vecregnum,V.vecdatepur,S.vecsrvnxtdate,S.vecsrvmilage,I.insduedate,R.roadnxtinpec,T.vectransto,T.vectranstodiv FROM vehicle V
    LEFT JOIN vecinsurance I ON I.vecregnum = V.vecregnum 
    LEFT JOIN vecservice S ON S.vecregnum = V.vecregnum
    LEFT JOIN vecroadworthy R ON R.vecregnum = V.vecregnum
    LEFT JOIN vechicletransfer T ON T.vecregnum = V.vecregnum";

	$result=pg_query($db,$sql);
	$count = pg_num_rows($result);
 ?>
 <!-- link for jquery scripts -->
 <!-- include jquery -->
    <script src="bootstrap/js/jquery.js"></script>
    <script src="bootstrap/js/jqueryui.js"></script>
 <script language="javascript">
  // ajax query for sending data for the search
function showHint(str) {
    if (str.length == 0) { 
       window.location.replace('report_vehicle.php');
    } else {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("txtHint").innerHTML = this.responseText;               
                            }
        };
        xmlhttp.open("GET", "report_vehicle_search.php?data=" + str, true);
        xmlhttp.send();
    }
}

</script>

 <h2 style="text-align: center;"><u>VEHICLES REPORT</u></h2>
 <!-- for showing of number or rows returned -->
 <div class="row">
        <div class="col-md-2">
          <u><h5 style="font-weight: bold;"><?php echo " Results found: ".$count; ?></h5></u>
        </div>
      </div>
 <div class="row">
    <!-- for search  -->
    <div class="col-md-10">
      <form action="#">
           <div class="form-group input-group">
              <input type="text" class="form-control" name="data" placeholder="searh here &hellip;" autocomplete="off" onkeyup="showHint(this.value)" autofocus="autofocus">
               <span class="input-group-btn"><button class="btn btn-default" disabled><i class="fa fa-search"></i></button></span>
            </div>
           </form>
    </div>
      <div class="col-md-2">
        <a href="javascript:Clickheretoprint()" class="btn btn-primary">PRINT</a>
        </div>
      </div>
        <div class="content" id="content">
          <div class="col-md-12">
            <h3 style="text-align: center;"><u>Vehicles Report Details</u></h3>
        </div>
 			<table class="table table-responsive table-condensed table-striped table-hover table-bordered" style="text-align: center;">
                <thead style="color: black;" class="main-color-bg"> 
                        <tr>  
                          <th style="border-bottom:1px solid #999999">REG NUMBER</th>
                          <th style="border-bottom:1px solid #999999">PURCHASED</th>
                          <th style="border-bottom:1px solid #999999">SERVICING</th>
                          <th style="border-bottom:1px solid #999999">MILAGE</th>
                          <th style="border-bottom:1px solid #999999">INSURANCE</th>
                          <th style="border-bottom:1px solid #999999">ROAD WORTHY</th>
                          <th style="border-bottom:1px solid #999999">USER</th>
                          <th style="border-bottom:1px solid #999999">TITLE</th>  
                        </tr>  
                </thead> 
                <tbody id="txtHint">
               <?php
                while($row=pg_fetch_array($result)){  //while look to fetch the result and store in a array $row.
                   	$vecregnum = trim($row['vecregnum']);
                    $vecdatepur = trim($row['vecdatepur']);
                    $insduedate = trim($row['insduedate']);
                    $vecsrvnxtdate = trim($row['vecsrvnxtdate']);
                    $vecsrvmilage = trim($row['vecsrvmilage']);
                    $roadnxtinpec = trim($row['roadnxtinpec']);  
                    $vectransto = trim($row['vectransto']);  
                    $vectranstodiv = trim($row['vectranstodiv']);  
                    ?> 
                     <tr> 
                        <td style="border-bottom:1px solid #999999"><?php echo $vecregnum;?></td>
                        <td style="border-bottom:1px solid #999999"><?php echo $vecdatepur;?></td>
                        <td style="border-bottom:1px solid #999999"><?php echo $vecsrvnxtdate;?></td>
                        <td style="border-bottom:1px solid #999999"><?php echo $vecsrvmilage;?></td>
                        <td style="border-bottom:1px solid #999999"><?php echo $insduedate;?></td>
                        <td style="border-bottom:1px solid #999999"><?php echo $roadnxtinpec;?></td>
                        <td style="border-bottom:1px solid #999999"><?php echo $vectransto;?></td>
                        <td style="border-bottom:1px solid #999999"><?php echo $vectranstodiv;?></td>
                      </tr>  
                       
                    <?php } ?> 
                  </tbody>
            </table>
          </div>
 <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
     <script src="cdn.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="bootstrap/js/bootstrap.min.js"></script>

  </body>
</html>

