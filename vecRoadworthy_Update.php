 <?php   
require('db/connection.php');
include("check.php"); 
$input=$_GET["data"];
$sql="SELECT * FROM vecroadworthy WHERE vecrowoid='$input'";
  $result=pg_query($db,$sql);
  $row=pg_fetch_array($result);
  if (!$result) {
     header("Location:vecRoadworthy.php");
  }else{
    $vecid = trim(strtoupper($row['vecid'])); 
    $roadamt=trim(strtoupper($row['roadamt'])); 
    $roadrecno=trim(strtoupper($row['roadrecno']));  
    $roadinspsdte=trim(strtoupper($row['roadinspsdate'])); 
    $roadsticknum=trim(strtoupper($row['roadsticknum'])); 
    $roadnxtinpec=trim(strtoupper($row['roadnxtinpec']));
    $inputrowid=trim(strtoupper($row['vecrowoid']));
    $vecregnum=trim(strtoupper($row['vecregnum']));  
  }
 ?>
 <script>
  function myFunction() {
    window.location.reload();
                      }
</script>

<div class="modal-header" id="bg">
        <button type="button" class="close" onClick="myFunction()" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">UPDATE ROADWORTHY</h4>
      </div>
<div class="modal-body" id="bg">
       <form method="POST" action="vecRoadworthy_UpdateScript.php"> 
                <div class="row">
                  <div class="col-md-6">
                    <!-- for vehicle name -->
                <div class="form-group">
                    <label for="regnum">VEHICLE REG:</label>
                    <input type="text" class="form-control" id="regnum"  value="<?php echo $vecregnum;?>" name="regnum" disabled>
                </div>
                <!-- for amount paid -->
               <label for="roadamt">AMOUNT PAID:</label>
                <div class="form-group input-group">
                  <span class="input-group-addon">₵</span>
                  <input type="text" class="form-control"  value="<?php echo $roadamt;?>" name="roadamt">
                  <span class="input-group-addon">.00</span>
                </div>
                  <!--receipt number  -->
                  <div class="form-group">
                    <label for="roadrecno">RECEIPT NUMBER:</label>
                    <input type="text" class="form-control" id="roadrecno"  value="<?php echo $roadrecno;?>" name="roadrecno" >
                </div>

                 <div  id="bg">
                    <a href="vecRoadworthy_report.php?data=<?php echo $vecregnum;?>" class="btn btn-md btn-default">PRINT<span class="glyphicon glyphicon-print"></span></a>
                 </div>
                  </div>

                  <div class="col-md-6">
                <!-- date of inpection -->
                <div class="form-group">
                    <label for="roadinpsdte">DATE OF INSPECTION:</label>
                    <input type="text" class="form-control" id="roadinpsdte"  value="<?php echo $roadinspsdte;?>" name="roadinpsdte" >
                </div>
                <!-- sticker number -->
          <div class="form-group">
                    <label for="roadsticknum">STICKER NUMBER:</label>
                    <input type="text" class="form-control" id="roadsticknum"  value="<?php echo $roadsticknum;?>" name="roadsticknum" >
                </div>
                <!-- NEXT INSPECTION DATE -->
          <div class="form-group">
                    <label for="roadnxtinpec">NEXT INPECTION DATE:</label>
                    <input type="text" class="form-control" id="roadnxtinpec"  value="<?php echo $roadnxtinpec;?>" name="roadnxtinpec" >
                </div>
                <div class="well modal-footer" id="bg">
              <input type="submit" class="btn btn-md btn-danger" name="submit" value="UPDATE ROADWORTHY " />
           </div>
              </div>
                </div>
            <!-- sending logedin userid -->
            <input name="inputrowid" value="<?php echo $inputrowid; ?>" hidden>
            <input name="user_id" value="<?php echo $_SESSION['loginid'] ?>" hidden>
              
        </form>
      </div>