<?php
include('check.php');
require('db/connection.php');
$inputid=$_GET["data"];
$sql="SELECT * FROM vecservice WHERE vecregnum='$inputid'";
$result=pg_query($db,$sql);
$row=pg_fetch_array($result);

$vecsrvinvoice = strtoupper($row['vecsrvinvoice']);
    $vecsrvdeal = strtoupper($row['vecsrvdeal']);
    $vecsrvdate = strtoupper($row['vecsrvdate']);
    $vecsrvnxtdate = strtoupper($row['vecsrvnxtdate']);
    $vecsrvnote = strtoupper($row['vecsrvnote']);
    $vecregnum = strtoupper($row['vecregnum']);
    $vecsrvmilage = strtoupper($row['vecsrvmilage']);
    
    require('fpdf/fpdf.php');
$pdf = new FPDF('P','mm','A4');
$pdf->AddPage();
// width,ss
$pdf->Image("images/printlogo.jpg",70,10,60,60);
// setting fonts using timees new rman
$pdf->SetFont('Times','B',12);
// line break
$pdf->Ln(60);
// for the page title
$pdf->SetFont('Times','BU',20);
$pdf->Cell(190,10,'MINISTRY OF LANDS AND NATURAL RESOURCES',0,1,'C');
$pdf->Cell(190,10,'VEHICLE MANAGEMENT APPLICATION',0,1,'C');
$pdf->Ln(5);
// for vechicle details
// setting leftmargin
$pdf->SetLeftMargin(30);
$pdf->Cell(60,10,"Vechicle servicing details:",0,1);
// for the body
$pdf->SetFont('Times','B',12);
// for reg number
$pdf->Cell(45,10,'REG NUMBER:',0,0);
$pdf->Cell(80,10,$vecregnum,1,1);
$pdf->Ln(5);

// for chasis number
$pdf->Cell(45,10,'SERVICE INVOICE:',0,0);
$pdf->Cell(80,10,$vecsrvinvoice,1,1);
$pdf->Ln(5);
// for make
$pdf->Cell(45,10,'DEALERSHIP:',0,0);
$pdf->Cell(80,10,$vecsrvdeal,1,1);
$pdf->Ln(5);
// for milage
$pdf->Cell(45,10,'MILAGE:',0,0);
$pdf->Cell(80,10,$vecsrvmilage,1,1);
$pdf->Ln(5);
// for model
$pdf->Cell(45,10,'SERVICING DATE:',0,0);
$pdf->Cell(80,10,$vecsrvdate,1,1);
$pdf->Ln(5);
// for premium
$pdf->Cell(45,10,'NEXT SERVICING:',0,0);
$pdf->Cell(80,10,$vecsrvnxtdate,1,1);
$pdf->Ln(5);
// for COLOR
$pdf->Cell(45,10,'NOTES:',0,0);
$pdf->Cell(80,10,$vecsrvnote,1,1);
$pdf->Ln(20);

$pdf->Cell(100,10,'.................................................',0,1);
$pdf->Cell(50,10,$displayname,0,1,'C');
$pdf->Output();
?>