<?php 
include("header.php");
// for insurance expiry dates showing
$currentdate = date("Y-m-d");
  $sql="SELECT * FROM vecinsurance";
  $insresult=pg_query($db,$sql) or die(pg_last_error());
  // for roadworth expiry
  $sql="SELECT * FROM vecroadworthy";
  $roadresult=pg_query($db,$sql) or die(pg_last_error());
   // for servicing due date
  $sql="SELECT * FROM vecservice";
  $servresult=pg_query($db,$sql) or die(pg_last_error());

  // for drivers licence expiry
  $sql="SELECT * FROM driverlicence";
  $licenresult=pg_query($db,$sql) or die(pg_last_error());
 ?>
<body>
<div class="row">
    <div class="col-md-3">
        <div class="panel panel-default">
              <div class="panel-heading main-color-bg">
                <h3 class="panel-title">INSURANCE EXPIRY</h3>
              </div>
              <div class="panel-body">
                <table class="table table-responsive table-condensed table-striped table-hover">
                <?php 
                    while($row=pg_fetch_array($insresult)){
                    $vecinsid=strtoupper($row['vecinsid']);
                    $insduedate=strtoupper($row['insduedate']);
                    $vecregnum=strtoupper($row['vecregnum']);
                    $currentdate = date("Y-m-d");
                    $d1 = new DateTime($insduedate);
                    $d2 = new DateTime($currentdate);
                    $interval = $d2->diff($d1);
                    $interval->format('%m months');
                    if(($interval->y ==0) && ($interval->m == 1)) {
                    ?>
                        <tr>
                          <td style="font-weight: bold;"><?php echo $vecregnum;?></td>
                          <td style="color: red;font-weight: bold;"><a href="vecInsurance_update.php?data=<?php echo $vecinsid;?>" data-toggle="modal" data-target="#updateModal" aria-label="Update"><?php echo $insduedate;?></a></td>
                        </tr>
                   <?php } ?>
                       <?php } ?> 
                </table>
              </div>
              </div>              
    </div>
    <div class="col-md-3">
        <div class="panel panel-default">
              <div class="panel-heading main-color-bg">
                <h3 class="panel-title">ROADWORTHY EXPIRY</h3>
              </div>
              <div class="panel-body">
                <table class="table table-responsive table-condensed table-striped table-hover">
                <?php 
                    while($row=pg_fetch_array($roadresult)){
                      $roadid=strtoupper($row['vecrowoid']);
                    $roadnxtinpec=strtoupper($row['roadnxtinpec']);
                    $vecregnum=strtoupper($row['vecregnum']);
                    $currentdate = date("Y-m-d");
                    $d1 = new DateTime($roadnxtinpec);
                    $d2 = new DateTime($currentdate);
                    $interval = $d2->diff($d1);
                    $interval->format('%m months');
                    if(($interval->y ==0) && ($interval->m == 1)) {
                    ?>
                        <tr>
                          <td style="font-weight: bold;"><?php echo $vecregnum;?></td>
                          <td style="color: red;font-weight: bold;"><a href="vecRoadworthy_update.php?data=<?php echo $roadid;?>" data-toggle="modal" data-target="#updateModal" aria-label="Update"><?php echo $roadnxtinpec;?></a></td>
                        </tr>
                    
                   <?php } ?>
                    
                       <?php } ?>    
                </table>      
              </div>
              </div>   
    </div>
    <div class="col-md-3">
        <div class="panel panel-default">
              <div class="panel-heading main-color-bg">
                <h3 class="panel-title">SERVICING EXPIRY</h3>
              </div>
              <div class="panel-body">
                <table class="table table-responsive table-condensed table-striped table-hover">
                <?php 
                    while($row=pg_fetch_array($servresult)){
                      $vecsrvid=strtoupper($row['vecsrvid']);
                    $vecsrvnxtdate=strtoupper($row['vecsrvnxtdate']);
                    $vecregnum=strtoupper($row['vecregnum']);
                    $currentdate = date("Y-m-d");
                    $d1 = new DateTime($vecsrvnxtdate);
                    $d2 = new DateTime($currentdate);
                    $interval = $d2->diff($d1);
                    $interval->format('%m months');
                    if(($interval->y ==0) && ($interval->m == 1)) {
                    ?>
                        <tr>
                          <td style="font-weight: bold;"><?php echo $vecregnum;?></td>
                          <td style="color: red;font-weight: bold;"><a href="vecService_Update.php?data=<?php echo $vecsrvid;?>" data-toggle="modal" data-target="#updateModal" aria-label="Update"><?php echo $vecsrvnxtdate;?></a></td>
                        </tr>
                    
                   <?php } ?>
                       <?php } ?>
                </table>
              </div>
              </div>   
    </div>
     <div class="col-md-3">
        <div class="panel panel-default">
              <div class="panel-heading main-color-bg">
                <h3 class="panel-title">LICENSE EXPIRY</h3>
              </div>
              <div class="panel-body">
                <table class="table table-responsive table-condensed table-striped table-hover">
                <?php 
                    while($row=pg_fetch_array($licenresult)){
                      $driverlinid=strtoupper($row['driverlinid']);
                    $licenexp=strtoupper($row['driverlinexp']);
                    $driverfirstname=strtoupper($row['driverfirstname']);
                    $currentdate = date("Y-m-d");
                    $d1 = new DateTime($licenexp);
                    $d2 = new DateTime($currentdate);
                    $interval = $d2->diff($d1);
                    $interval->format('%m months');
                    if(($interval->y ==0) && ($interval->m == 1)) {
                    ?>
                        <tr>
                          <td style="font-weight: bold;"><?php echo $driverfirstname;?></td>
                          <td style="color: red;font-weight: bold;"><a href="vecDriversLin_Update.php?data=<?php echo $driverlinid;?>" data-toggle="modal" data-target="#updateModal" aria-label="Update"><?php echo $licenexp;?></a></td>
                        </tr>
                    
                   <?php } ?>
                       <?php } ?>
                </table>
              </div>
              </div>   
    </div>
</div>
<!-- for teh updat modal -->
<div class="modal fade" id="updateModal" tabindex="-1" role="dialog" aria-labelledby="updateModalLabel" aria-hidden="true">
  <div class="modal-dialog ">
    <div class="modal-content">
    </div>
  </div>
</div>
<!-- delete above this line -->
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
     <script src="cdn.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="bootstrap/js/bootstrap.min.js"></script>

  </body>
</html>
