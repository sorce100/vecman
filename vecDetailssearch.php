<?php 
include("header.php");
require('db/connection.php');
	$inputdata = strtoupper($_POST['data']);

 $sql="SELECT * FROM vehicle WHERE CONCAT(vecmanufacturer,vecmodel,vecchasis,veccolor,vecseat,vecfuel,vecregnum,veccondition,vecregion,vecregnum) LIKE '%".$inputdata."%'";
$result=pg_query($db,$sql);
  
 ?>
 
	<div class="row">
		<!-- for add button and search button -->
		<div class="col-md-2">
			<button data-toggle="modal" data-target="#myModal" class="btn btn-danger"><span class="glyphicon glyphicon-plus"></span> ADD NEW</button>
		</div>
		<!-- for search  -->
		<div class="col-md-10">
		  <form action="vecDetailssearch.php" method="POST">
			<div class="form-group input-group">
              <input type="text" class="form-control" placeholder="searh here &hellip;" name="data">
               <span class="input-group-btn"><button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button></span>
            </div>
           </form>
		</div>
	</div>
	<!-- div for content -->
	<div class="row">
		<div class="col-lg-12">
          <div class="panel panel-default">
              <div class="panel-heading main-color-bg">
                <h3 class="panel-title">SEARCH DETAILS</h3>
              </div>
              <div class="panel-body">
                
                <table class="table table-responsive table-condensed table-striped table-hover">
                <thead style="color: black;">    
                        <tr>  
                          <th>REG NUMBER</th>
                          <th>MAKE</th>  
                          <th>MODEL</th>
                          <th>CONDITION</th> 
                          <th>FUEL TYPE</th>
                          <th>REGION</th>
                        </tr>  
                    </thead> 
               <?php
                while($row=pg_fetch_array($result)){  //while look to fetch the result and store in a array $row.
                   $vec_id=strtoupper($row['vecid']); 
                      $vecregnum=trim(strtoupper($row['vecregnum'])); 
                      $vecmanu=trim(strtoupper($row['vecmanufacturer']));  
                      $vecmodel=trim(strtoupper($row['vecmodel'])); 
                      $veccon=trim(strtoupper($row['veccondition'])); 
                      $vecfuel=trim(strtoupper($row['vecfuel']));  
                      $vecregion=trim(strtoupper($row['vecregion']));  
                    ?> 
                    <tr> 
                      <td><?php echo $vecregnum;?></td>
                      <td><?php echo $vecmanu;?></td>
                      <td><?php echo $vecmodel;?></td> 
                      <td><?php echo $veccon;?></td>
                      <td><?php echo $vecfuel;?></td>
                      <td><?php echo $vecregion;?></td>
                      <td>
                        <a href="vecDetails_update.php?data=<?php echo $vec_id;?>" data-toggle="modal" data-target="#updateModal" class="btn-sm btn-success" aria-label="Update">
                          <i class="fa fa-pencil fa-fw" aria-hidden="true"></i>
                        </a>
                      </td>
                      <td>
                        <a href="vecDetails_del.php?data=<?php echo $vec_id;?>" class="btn-sm btn-danger" aria-label="Delete" onclick="return confirm('Are you sure to delete !'); ">
                          <i class="fa fa-trash-o" aria-hidden="true"></i>
                        </a>
                      </td>
                    </tr>  
                    <?php } ?> 
                    </table> 
              </div>
              </div>              
        </div>
	</div>
 </div>
<!-- modal for saving -->
 <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" id="bg">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">ADD NEW VEHICLE</h4>
      </div>
      <div class="modal-body" id="bg">
      	<form method="POST" action="vecDetails_save.php"> 
            <div class="row">
            	<div class="col-md-6">
            		<div class="form-group">
                		<label for="vecregion">REGION:</label>
                		<select name="vecregion" class="form-control">
                              <option value="Greater Accra">Greater Accra</option>
                              <option value="Eastern">Eastern</option>
                              <option value="Ashanti">Ashanti</option>
                              <option value="Central">Central</option>
                              <option value="Western">Western</option>
                              <option value="volta">Volta</option>
                              <option value="Northern">Northern</option>
                             <option value="Upper East">Upper East</option>
                              <option value="Upper West">Upper West</option>
                          </select>
            		</div>
            	<!-- for manufacturer -->
            		<div class="form-group">
                		<label for="manufacturer">MAKE:</label>
                		<select name="vecmanu" class="form-control">
                               <option value="toyota">Toyota</option>
                               <option value="nissan">Nissan</option>
                               <option value="ford">Ford</option>
                               <option value="mitsubishi">Mitsubishi</option>
                                <option value="honda">Honda</option>
                           </select>
            		</div>
            		<!-- for model -->
            		<div class="form-group">
                		<label for="vecModel">MODEL:</label>
                		<input type="text" class="form-control" id="VecModel" placeholder="Enter Vehicle model" name="vecmodel" required>
            		</div>
            		<!-- for for color -->
            		<div class="form-group">
                		<label for="vecColor">COLOR:</label>
                		<select name="veccolor" class="form-control">
                               <option value="silver">Silver</option>
                               <option value="blue">Blue</option>
                               <option value="white">White</option>
                               <option value="green">Green</option>
                                <option value="black">Black</option>
                           </select>
            		</div>
            		<!-- for chasis num -->
            		<div class="form-group">
                		<label for="chasisNo">CHASIS NUMBER:</label>
                		<input type="text" class="form-control" id="chasisNo" placeholder="Enter Chasis Number;" name="vecchasis" required>
            		</div>
            		<!-- for searing capacity -->
            		<div class="form-group">
                		<label for="seatingCapa">SEATING CAPACITY:</label>
                		<input type="number" class="form-control" id="seatingCapa" placeholder="Enter Seating capacity" name="vecseat">
            		</div>
            		
            		
            	</div>
            	<div class="col-md-6">
                <!-- for manufacturer -->
                <div class="form-group">
                    <label for="vecFuel">FUEL TYPE:</label>
                           <select name="vecfuel" class="form-control">
                               <option value="petrol">Petrol</option>
                               <option value="desiel">Desiel</option>
                           </select>
                </div>
            		<!-- for condition of car -->
            		<div class="form-group">
                		<label for="vecpurchase">CONDITION:</label>
                		<select name="veccondition" class="form-control">
                               <option value="new">New</option>
                               <option value="used">Used</option>
                           </select>
            		</div>
            		<!-- for date of purchase -->
            		<div class="form-group">
                		<label for="vecpurchase">DATE PURCHASED:</label>
                		<input type="date" id="datepicker" name="vecdatepur" class="form-control">
            		</div>
            		<div class="form-group">
                		<label for="vecmilage">VEHICLE MILAGE:</label>
                		<input type="text" class="form-control" id="vecmilage" placeholder="Enter Miliage;" name="vecliage" required>
            		</div>
            		<!-- for registration number -->
            		<div class="form-group">
                		<label for="vecReg">REGISTRATION NUMBER:</label>
                		<input type="text" class="form-control" id="vecReg" placeholder="Enter Registration Number" name="vecreg" required>
            		</div>
            	</div>
            </div>
            <!-- sending logedin userid -->
            <input name="user_id" value="<?php echo $_SESSION['loginid'] ?>" hidden>
              <div class="well modal-footer" id="bg">
            	<input type="submit" class="btn btn-md btn-danger" name="submit" value="ADD VEHICLE" />
       		 </div>
        </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- for teh updat modal -->
<div class="modal fade" id="updateModal" tabindex="-1" role="dialog" aria-labelledby="updateModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    </div>
  </div>
</div>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
     <script src="cdn.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="bootstrap/js/bootstrap.min.js"></script>

  </body>
</html>
