<?php   
require('db/connection.php');
$userid=$_GET["data"];
$sql="SELECT * FROM users WHERE userid='$userid'";
  $result=pg_query($db,$sql);
  $row=pg_fetch_array($result);

  if (!$result) {
     header("Location:users.php");
  }else{
    $userid=$row['userid'];
    $fname=trim(strtoupper($row['fname'])); 
    $lname=trim(strtoupper($row['lname']));  
    $uname=trim(strtoupper($row['username']));
    $status=trim(strtoupper($row['status']));
    $email=trim(strtoupper($row['email']));
    $rank=trim(strtoupper($row['rank']));
    // retriving password for update
    $password=trim($row['password']);
  }
 ?>
 <script>
  function myFunction() {
    window.location.reload();
                      }
</script>

     <div class="modal-header" id="bg">
        <button type="button" class="close" onclick="myFunction()"; data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">UPDATE USER</h4>
      </div>
      <div class="modal-body" id="bg">
     <form method="POST" action="user_updateScript.php"> 
          <div class="row">
            <!-- left side -->
            <div class="col-md-6">
              <div class="form-group">
                <label for="firstname">FIRST NAME</label>
                <input type="text" class="form-control" id="firstname" value="<?php echo $fname;?>" name="fname" required>
            </div>
            <div class="form-group">
                <label for="username">USER NAME</label>
                <input type="text" class="form-control" id="username" value="<?php echo $uname;?>" name="username">
            </div>

            <div class="form-group">
                <label for="email">EMAIL</label>
                <input type="email" class="form-control" id="email" value="<?php echo $email;?>" name="email">
            </div>
            <div class="form-group">
                <label for="status">STATUS</label>
                        <select name="status" class="form-control">
                           <option value="<?php echo $status;?>"><?php echo $status;?></option>
                              <option disabled>----------</option>
                          <option value="active">ACTIVE</option>
                          <option value="inactive">INACTIVE</option>
                      </select>
            </div>
            </div>
            <!-- right side -->
            <div class="col-md-6">
              <div class="form-group">
                <label for="lastname ">LAST NAME</label>
                <input type="text" class="form-control" id="lastname" value="<?php echo $lname;?>" name="lname" required>
            </div>
            <div class="form-group">
                <label for="conflict">PASSWORD</label>
                <input type="password" class="form-control" id="password" name="password">
            </div>
<div class="form-group">
                <label for="rank">RANK</label>
                        <select name="rank" class="form-control">
                           <option value="<?php echo $rank;?>"><?php echo $rank;?></option>
                              <option disabled>----------</option>
                          <option value="officer">OFFICER</option>
                          <option value="admin">ADMINISTRATOR</option>
                      </select>
            </div>
            </div>
          </div>
         
            <input name="inputuserid" value="<?php echo $userid; ?>" hidden>
            <div class="well modal-footer" id="bg">
            <input type="submit" class="btn btn-danger" name="submit" value="UPDATE USER" />
        </div>
        </form>
      </div>
  