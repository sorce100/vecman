<?php
	require("db/connection.php");
	require('auth.php');
	// retriving input data
		$inputvecid = trim(strtoupper($_POST["inputvecid"]));
		$vecregion = trim(strtoupper($_POST["vecregion"]));
		$vecmanu = trim(strtoupper($_POST["vecmanu"]));
		$vecmodel = trim(strtoupper($_POST["vecmodel"]));
		$veccolor = trim(strtoupper($_POST["veccolor"]));
		$vecchasis = trim(strtoupper($_POST["vecchasis"]));
		$vecseat = trim(strtoupper($_POST["vecseat"]));
		$vecfuel = trim(strtoupper($_POST["vecfuel"]));
		$vecmilage = trim(strtoupper($_POST["vecmilage"]));
		$veccondition = trim(strtoupper($_POST["veccondition"]));
		$vecdatepur = trim(strtoupper($_POST["vecdatepur"]));
		$vecreg = trim(strtoupper($_POST["vecreg"]));		
// checking against sql injection
		$vecregion = pg_escape_string($db, $vecregion);
		$vecmanu = pg_escape_string($db, $vecmanu);
		$vecmodel = pg_escape_string($db, $vecmodel);
		$veccolor = pg_escape_string($db, $veccolor);
		$vecchasis = pg_escape_string($db, $vecchasis);
		$vecseat = pg_escape_string($db, $vecseat);
		$vecfuel = pg_escape_string($db, $vecfuel);
		$vecmilage = pg_escape_string($db, $vecmilage);
		$vecreg = pg_escape_string($db, $vecreg);
		$veccondition = pg_escape_string($db, $veccondition);
		$vecdatepur = pg_escape_string($db, $vecdatepur);
// retriecing details of image uploaded for the update
		if(isset($_POST['submit'])){
			$file = $_FILES['file'];
			// full name of the file
			$filename = $_FILES['file']['name'];
			// temporal location of the file
			$filetmpname = $_FILES['file']['tmp_name'];
			// size of the file#
			$filesize = $_FILES['file']['size'];
			// error
			$fileerror = $_FILES['file']['error'];
			// type of file being uploaded
			$filetype = $_FILES['file']['type'];
		}
// retrieving data for use for comparism for the update made
		$sql ="SELECT * FROM vehicle WHERE vecid='".$inputvecid."'";
		$result=pg_query($db,$sql);
		$row=pg_fetch_array($result);
		$returnvecid=$row["vecid"];
		$returnvecimage=$row["vecimage"];
// the update query by storing the name of the image if no update is being made to the origianl content
	if ($fileerror==4){
		if ($returnvecid==$inputvecid) {
			$sql="UPDATE vehicle SET vecmanufacturer='".$vecmanu."',vecmodel='".$vecmodel."',vecseat='".$vecseat."',veccolor='".$veccolor."',vecchasis='".$vecchasis."',vecfuel='".$vecfuel."',vecmilage='".$vecmilage."',veccondition='".$veccondition."',vecregion='".$vecregion."',vecregnum='".$vecreg."',vecdatepur='".$vecdatepur."',vecimage='".$returnvecimage."' WHERE vecid='".$inputvecid."'" ;
				$result=pg_query($db,$sql);
			if ($result) {
				header("Location:vecDetails.php");	
			 }
			 else{
			 	$msg='Oops!there was a problem updating';

			 	echo "<SCRIPT type='text/javascript'>alert('$msg'); window.location.replace('vecDetails.php');</SCRIPT>";
			 	
			 }
			}
			else{
				$msg='Oops!there was a problem updating';

			 	echo "<SCRIPT type='text/javascript'>alert('$msg'); window.location.replace('vecDetails.php');</SCRIPT>";
			 				}
			 				// for when an update is being made
			 	}else{
	// which files to allow to upload into the database
	// explose name to get the actual name and the extension thus differentiating using the dot before the extension
	$fileExt = explode('.', $filename);
	// filename
	$fileActualName= current($fileExt);
	// we get two parts hers thus the name of the file and
	// end() get the last piece of information from an array
	$fileActualExt = strtolower(end($fileExt));
	// specifying file to allow to be uploaded
	$allowed=array('jpg','jpeg','png');
if (in_array($fileActualExt,$allowed)) {
		// check if ther is any errors from the uploaded file
		if ($fileerror === 0) {
			// check if file uploaded has ther righ size
			if ($filesize < 10000000000000000) {
				// creating unique ids for uploads instead of names of files, number is in microsecond

				$filenamenew=$fileActualName.".".$fileActualExt;
				$filedestination = 'uploads/'.$filenamenew;
				// moving file from temporal location to permanent storage
				$fileup=move_uploaded_file($filetmpname,$filedestination);
				if ($fileup==1) {
					if ($returnid==$id) {
						$sql="UPDATE vehicle SET vecmanufacturer='".$vecmanu."',vecmodel='".$vecmodel."',vecseat='".$vecseat."',veccolor='".$veccolor."',vecchasis='".$vecchasis."',vecfuel='".$vecfuel."',vecmilage='".$vecmilage."',veccondition='".$veccondition."',vecregion='".$vecregion."',vecregnum='".$vecreg."',vecdatepur='".$vecdatepur."',vecimage='".$filenamenew."' WHERE vecid='".$inputvecid."'" ;
					$result=pg_query($db,$sql);
	if ($result) {
				header("Location:vecDetails.php");	
			 }
			 else{
			 	$msg='Oops!there was a problem updating 3';

			 	echo "<SCRIPT type='text/javascript'>alert('$msg'); window.location.replace('vecDetails.php');</SCRIPT>";
			 	return;
			 }
			}
						}
				
						}

					}
					else{
						$msg='Oops! Something went wrong';
						echo "<SCRIPT type='text/javascript'>alert('$msg');window.location.replace('vecDetails.php');</SCRIPT>";
					}
				}
			}
?>