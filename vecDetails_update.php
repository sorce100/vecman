 
 <?php   
require('db/connection.php');
$input=$_GET["data"];

$sql="SELECT * FROM vehicle WHERE vecid='$input'";
  $result=pg_query($db,$sql);
  $row=pg_fetch_array($result);
  if (!$result) {
     header("Location:vecDetails.php");
  }else{
    $vecid=trim(strtoupper($row['vecid'])); 
    $vecmanufacturer=trim(strtoupper($row['vecmanufacturer']));  
    $vecmodel=trim(strtoupper($row['vecmodel'])); 
    $vecchasis=trim(strtoupper($row['vecchasis'])); 
    $veccolor=trim(strtoupper($row['veccolor'])); 
    $vecseat=trim(strtoupper($row['vecseat']));
    $vecfuel=trim(strtoupper($row['vecfuel'])); 
    $vecreg=trim(strtoupper($row['vecregnum'])); 
    $vecmilage=trim(strtoupper($row['vecmilage'])); 
    $vecregion=trim(strtoupper($row['vecregion']));  
    $veccondition=trim(strtoupper($row['veccondition']));
    $vecdatepur=trim(strtoupper($row['vecdatepur']));
    $vecregnum=trim(strtoupper($row['vecregnum']));  
  }
 ?>
 <!-- links for tcalender -->
 <script>
  function myFunction() {
    window.location.reload();
                      }
</script>
  <div class="modal-header" id="bg">
        <button type="button" class="close" data-dismiss="modal" onClick="myFunction()" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">UPDATE VEHICLE</h4>
      </div>
 <div class="modal-body" id="bg">
        <form method="POST" action="vecDetails_UpdateScript.php" enctype="multipart/form-data"> 
            <div class="row">
              <!-- col for displaying images -->
              <div class="col-md-4">
                <br>
                 <div class="row">
                   <?php echo '<img src="uploads/'.$row['vecimage'] .'" height="100%" width="100%"/>';?>
                 </div>
              </div>
              <!--  -->
              <div class="col-md-4">
                <div class="form-group">
                    <label for="vecregion">REGION:</label>
                    <select name="vecregion" class="form-control">
                              <option value="<?php echo $vecregion;?>"><?php echo $vecregion;?></option>
                              <option disabled>----------</option>
                              <option value="Greater Accra">Greater Accra</option>
                              <option value="Eastern">Eastern</option>
                              <option value="Ashanti">Ashanti</option>
                              <option value="Central">Central</option>
                              <option value="Western">Western</option>
                              <option value="volta">Volta</option>
                              <option value="Northern">Northern</option>
                             <option value="Upper East">Upper East</option>
                              <option value="Upper West">Upper West</option>
                          </select>
                </div>
              <!-- for manufacturer -->
                <div class="form-group">
                    <label for="manufacturer">MAKE:</label>
                    <select name="vecmanu" class="form-control">
                               <option value="<?php echo $vecmanufacturer;?>"><?php echo $vecmanufacturer;?></option>
                               <option disabled>----------</option>
                               <option value="toyota">Toyota</option>
                               <option value="nissan">Nissan</option>
                               <option value="ford">Ford</option>
                               <option value="mitsubishi">Mitsubishi</option>
                                <option value="honda">Honda</option>
                           </select>
                </div>
                <!-- for model -->
                <div class="form-group">
                    <label for="vecModel">MODEL:</label>
                    <input type="text" class="form-control" id="VecModel" value="<?php echo $vecmodel;?>" name="vecmodel">
                </div>
                <!-- for for color -->
                <div class="form-group">
                    <label for="vecColor">COLOR:</label>
                    <select name="veccolor" class="form-control">
                              <option value="<?php echo $veccolor;?>"><?php echo $veccolor;?></option>
                               <option disabled>----------</option>
                               <option value="silver">Silver</option>
                               <option value="blue">Blue</option>
                               <option value="white">White</option>
                               <option value="green">Green</option>
                                <option value="black">Black</option>
                           </select>
                </div>
                <!-- for chasis num -->
                <div class="form-group">
                    <label for="chasisNo">CHASIS NUMBER:</label>
                    <input type="text" class="form-control" id="chasisNo" value="<?php echo $vecchasis;?>" name="vecchasis" >
                </div>
                <!-- for searing capacity -->
                <div class="form-group">
                    <label for="seatingCapa">SEATING CAPACITY:</label>
                    <input type="number" class="form-control" id="seatingCapa" value="<?php echo $vecseat;?>" name="vecseat">
                </div>
                   <div  id="bg">
                    <a href="vecDetails_report.php?data=<?php echo $vecregnum;?>" class="btn btn-md btn-default">PRINT <span class="glyphicon glyphicon-print"></span></a>
                 </div>
              </div>
              <div class="col-md-4">
                <!-- for manufacturer -->
                <div class="form-group">
                    <label for="vecFuel">FUEL TYPE:</label>
                           <select name="vecfuel" class="form-control">
                              <option value="<?php echo $vecfuel;?>"><?php echo $vecfuel;?></option>
                               <option disabled>----------</option>
                               <option value="petrol">Petrol</option>
                               <option value="desiel">Desiel</option>
                           </select>
                </div>
                <!-- for condition of car -->
                <div class="form-group">
                    <label for="vecpurchase">CONDITION:</label>
                    <select name="veccondition" class="form-control">
                               <option value="<?php echo $veccondition;?>"><?php echo $veccondition;?></option>
                               <option disabled>----------</option>
                               <option value="new">New</option>
                               <option value="used">Used</option>
                           </select>
                </div>
                <!-- for date of purchase -->
                <div class="form-group">
                    <label for="vecpurchase">DATE PURCHASED:</label>
                    <input type="text"  name="vecdatepur" value="<?php echo $vecdatepur;?>"  class="form-control tcal">
                </div>
                <div class="form-group">
                    <label for="vecmilage">VEHICLE MILAGE:</label>
                    <input type="text" class="form-control" id="vecmilage" value="<?php echo $vecmilage;?>" name="vecmilage">
                </div>
                <!-- for registration number -->
                <div class="form-group">
                    <label for="vecReg">REGISTRATION NUMBER:</label>
                    <input type="text" class="form-control" id="vecReg" value="<?php echo $vecreg;?>" name="vecreg" >
                </div>
               <!-- for file registertation -->
                <div class="form-group">
                    <label for="picture">UPDATE IMAGE:</label>
                       <input type="file" name="file" class="form-control"/>
                </div>
            </div>
             <div class="well modal-footer" id="bg">
                  <input type="submit" class="btn btn-md btn-danger" name="submit" value="UPDATE VEHICLE" />
                </div>
              </div>
            <!-- sending logedin userid -->
            <input name="inputvecid" value="<?php echo $vecid ?>" hidden>
              
        </form>
      </div>