<?php
include('check.php');
require('db/connection.php');
$inputid=$_GET["data"];
$sql="SELECT * FROM vehicle WHERE vecregnum='$inputid'";
$result=pg_query($db,$sql);
$row=pg_fetch_array($result);

$vecmanufacturer=strtoupper($row['vecmanufacturer']);  
    $vecmodel=strtoupper($row['vecmodel']); 
    $vecchasis=strtoupper($row['vecchasis']); 
    $veccolor=strtoupper($row['veccolor']); 
    $vecseat=strtoupper($row['vecseat']);
    $vecfuel=strtoupper($row['vecfuel']); 
    $vecreg=strtoupper($row['vecregnum']); 
    $vecmilage=strtoupper($row['vecmilage']); 
    $vecregion=strtoupper($row['vecregion']);  
    $veccondition=strtoupper($row['veccondition']);
    $vecdatepur=strtoupper($row['vecdatepur']);
    $vecregnum=strtoupper($row['vecregnum']); 
    $vecmanu=strtoupper($row['vecmanufacturer']); 

require('fpdf/fpdf.php');
$pdf = new FPDF('P','mm','A4');

$pdf->AddPage();

// width,ss
$pdf->Image("images/printlogo.jpg",70,10,60,60);
// setting fonts using timees new rman
$pdf->SetFont('Times','B',12);
// line break
$pdf->Ln(60);
// for the page title
$pdf->SetFont('Times','BU',20);
$pdf->Cell(190,10,'MINISTRY OF LANDS AND NATURAL RESOURCES',0,1,'C');
$pdf->Cell(190,10,'VEHICLE MANAGEMENT APPLICATION',0,1,'C');
$pdf->Ln(5);
// for vechicle details
// setting leftmargin
$pdf->SetLeftMargin(30);
$pdf->Cell(60,10,"Vechicle details:",0,1);
// for the body
$pdf->SetFont('Times','B',12);
// for reg number
$pdf->Cell(45,10,'REG NUMBER:',0,0);
$pdf->Cell(80,10,$vecregnum,1,1);
$pdf->Ln(5);

// for chasis number
$pdf->Cell(45,10,'CHASIS:',0,0);
$pdf->Cell(80,10,$vecchasis,1,1);
$pdf->Ln(5);
// for make
$pdf->Cell(45,10,'MAKE:',0,0);
$pdf->Cell(80,10,$vecmanu,1,1);
$pdf->Ln(5);

// for model
$pdf->Cell(45,10,'MODEL:',0,0);
$pdf->Cell(80,10,$vecmodel,1,1);
$pdf->Ln(5);
// for COLOR
$pdf->Cell(45,10,'COLOR:',0,0);
$pdf->Cell(80,10,$veccolor,1,1);
$pdf->Ln(5);

// for MILAGE
$pdf->Cell(45,10,'MILAGE:',0,0);
$pdf->Cell(80,10,$vecmilage,1,1);
$pdf->Ln(5);
// for seating
$pdf->Cell(45,10,'SEATING:',0,0);
$pdf->Cell(80,10,$vecseat,1,1);
$pdf->Ln(5);
// for fuel type
$pdf->Cell(45,10,'FUEL TYPE:',0,0);
$pdf->Cell(80,10,$vecfuel,1,1);
$pdf->Ln(5);
// for seating
$pdf->Cell(45,10,'DATE PURCHASED:',0,0);
$pdf->Cell(80,10,$vecdatepur,1,1);
$pdf->Ln(20);

$pdf->Cell(100,10,'.................................................',0,1);
$pdf->Cell(50,10,$displayname,0,1,'C');
$pdf->Output();
?>