<?php 
include("header.php");
require('auth.php');
require('db/connection.php');
$inputdata = strtoupper($_POST["data"]);
$sql="SELECT * FROM users WHERE CONCAT(fname,lname,username,status,rank,email) LIKE '%".$inputdata."%'";
$result=pg_query($db,$sql);
 ?>
 <div class="row">
    <!-- for add button and search button -->
    <div class="col-md-2">
      <button data-toggle="modal" data-target="#myModal" class="btn btn-danger"><span class="glyphicon glyphicon-plus"></span> ADD USER</button>
    </div>
    <!-- for search  -->
    <div class="col-md-10">
      <form action="usersearch.php" method="POST">
      <div class="form-group input-group">
              <input type="text" class="form-control" placeholder="searh here &hellip;" name="data">
               <span class="input-group-btn"><button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button></span>
            </div>
           </form>
    </div>
  </div>
  <!-- div for content -->
  <div class="row">
    <div class="col-lg-12">
          <div class="panel panel-default">
              <div class="panel-heading main-color-bg">
                <h3 class="panel-title">ACCOUNT USER SEARCH DETAILS</h3>
              </div>
              <div class="panel-body">
                
                <table class="table table-responsive table-condensed table-striped table-hover">
                <thead style="color: black;">    
                        <tr> 
                          <th>FIRST NAME</th>
                          <th>LAST NAME</th>  
                          <th>USERNAME</th>
                          <th>RANK</th>
                          <th>STATUS</th> 
                        </tr>  
                    </thead> 
               <?php
                while($row=pg_fetch_array($result)){  //while look to fetch the result and store in a array $row.
                      $fname=trim(strtoupper($row['fname'])); 
						          $lname=trim(strtoupper($row['lname'])); 
						          $uname=trim(strtoupper($row['username']));  
						          $status=trim(strtoupper($row['status'])); 
						           $rank=trim(strtoupper($row['rank'])); 
						          $email=trim(strtoupper($row['email'])); 
						          $userid=trim(strtoupper($row['userid'])); 
                    ?> 
                    <tr> 
                      <td><?php echo $fname;?></td>
                      <td><?php echo $lname;?></td>
                      <td><?php echo $uname;?></td> 
                      <td><?php echo $rank;?></td>
                      <td><?php echo $status;?></td>
                      <td>
                         <a href="users_update.php?data=<?php echo $userid;?>" class="btn btn-success btn-xs" data-toggle="modal" data-target="#updateModal" style="color:white;"><span class="glyphicon glyphicon-cog"></span></a>
                      </td>
                    </tr>  
                    <?php } ?> 
                    </table> 
              </div>
              </div>              
        </div>
  </div>
 </div>

 <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog ">
    <div class="modal-content">
      <div class="modal-header" id="bg">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">ADD NEW USER</h4>
      </div>
      <div class="modal-body" id="bg">
     <form method="POST" action="user_save.php"> 
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="firstname">FIRST NAME</label>
                <input type="text" class="form-control" id="firstname" placeholder="Enter firstname &hellip;" name="fname" required>
            </div>
            <div class="form-group">
                <label for="lastname ">LAST NAME</label>
                <input type="text" class="form-control" id="lastname" placeholder="Enter lastname  &hellip;" name="lname" required>
            </div>
            <div class="form-group">
                <label for="username">USER NAME</label>
                <input type="text" class="form-control" id="username" placeholder="Enter username for login &hellip;" name="username">
            </div>
            
            <div class="form-group">
                <label for="rank">RANK</label>
                        <select name="rank" class="form-control">
                          <option value="officer">OFFICER</option>
                          <option value="admin">ADMINISTRATOR</option>
                      </select>
            </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="firstname">FIRST NAME</label>
                <input type="text" class="form-control" id="firstname" placeholder="Enter firstname &hellip;" name="fname" required>
            </div>
            <div class="form-group">
                <label for="email ">EMAIL</label>
                <input type="email" class="form-control" id="email" placeholder="Enter email  &hellip;" name="email" required>
            </div>
            <div class="form-group">
                <label for="conflict">PASSWORD</label>
                <input type="password" class="form-control" id="password" placeholder="Enter password for login &hellip;" name="password">
            </div>
            <div class="well modal-footer" id="bg">
            <input type="submit" class="btn btn-danger" name="submit" value="ADD USER" />
        </div>
            </div>
          </div>
            <input name="creator" value="<?php echo $_SESSION['displayname'] ?>" hidden>
            
        </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
</div>
<!-- for teh updat modal -->
<div class="modal fade" id="updateModal" tabindex="-1" role="dialog" aria-labelledby="updateModalLabel" aria-hidden="true">
  <div class="modal-dialog ">
    <div class="modal-content">
    </div>
  </div>
</div>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
     <script src="cdn.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="bootstrap/js/bootstrap.min.js"></script>

  </body>
</html>
