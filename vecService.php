<?php 
include("header.php");
require('db/connection.php');
	$sql="SELECT * FROM vecservice ORDER BY date_done DESC";
	$result=pg_query($db,$sql) or die(pg_last_error());
	$row=pg_fetch_array($results);
 ?>
 <script>
function showHint(str) {
    if (str.length == 0) { 
        document.getElementById("txtHint").innerHTML = "";
        return;
    } else {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("txtHint").innerHTML = this.responseText;
                  
                            }
        };
        xmlhttp.open("GET", "vecRegCheck.php?data=" + str, true);
        xmlhttp.send();
    }
}
// function for disabling and enabling the button
 function myFunction() {
    window.location.reload();
                      }

</script>
	<div class="row">
		<!-- for add button and search button -->
		<div class="col-md-2">
			<button data-toggle="modal" data-target="#myModal" class="btn btn-danger"><span class="glyphicon glyphicon-plus"></span> ADD NEW</button>
		</div>
		<!-- for search  -->
		<div class="col-md-10">
		  <form action="vecServicesearch.php" method="POST">
			<div class="form-group input-group">
              <input type="text" class="form-control" placeholder="searh here &hellip;" name="data">
               <span class="input-group-btn"><button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button></span>
            </div>
           </form>
		</div>
	</div>
	<!-- div for content -->
	<div class="row">
		<div class="col-lg-12">
          <div class="panel panel-default">
              <div class="panel-heading main-color-bg">
                <h3 class="panel-title">VEHICLE SERVICING DETAILS</h3>
              </div>
              <div class="panel-body">
                
                <table class="table table-responsive table-condensed table-striped table-hover">
                <thead style="color: black;">    
                        <tr> 
                          <th>REG NUMBER</th>
                          <th>NEXT SERVICING</th>
                          <th>MILAGE</th>
                          <th>DEALERSHIP</th>
                        </tr>  
                    </thead> 
               <?php
                while($row=pg_fetch_array($result)){  //while look to fetch the result and store in a array $row.
                      $vecsrvid=trim(strtoupper($row['vecsrvid']));
                      $vecid=trim(strtoupper($row['vecid'])); 
                      $vecsrvnxtdate=trim(strtoupper($row['vecsrvnxtdate']));  
                      $vecsrvdeal=trim(strtoupper($row['vecsrvdeal']));
                      $vecsrvmilage=trim(strtoupper($row['vecsrvmilage']));  
                      $vecreg=trim(strtoupper($row['vecregnum']));  
                    ?> 
                    <tr> 
                      <td><?php echo $vecreg;?></td>
                      <td><?php echo $vecsrvnxtdate;?></td>
                      <td><?php echo $vecsrvmilage;?></td>
                      <td><?php echo $vecsrvdeal;?></td> 
                      <td>
                        <a href="vecService_Update.php?data=<?php echo $vecsrvid;?>" data-toggle="modal" data-target="#updateModal" class="btn-sm btn-success" aria-label="Update">
                          <i class="fa fa-pencil fa-fw" aria-hidden="true"></i>
                        </a>
                      </td>
                      
                      <td>
                        <a href="vecService_del.php?data=<?php echo $vecsrvid;?>" class="btn-sm btn-danger" aria-label="Delete" onclick="return confirm('Are you sure to delete !'); ">
                          <i class="fa fa-trash-o" aria-hidden="true"></i>
                        </a>
                      </td>
                    </tr>  
                    <?php } ?> 
                    </table> 
              </div>
              </div>              
        </div>
	</div>
 </div>
<!-- modal for saving -->
 <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" id="bg">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" onClick="myFunction()"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">ADD VEHICLE SERVICING</h4>
      </div>
      <div class="modal-body" id="bg">
      	<form method="POST" action="vecService_save.php"> 
            		<div class="row">
                  <div class="col-md-6">
                    <div class="row">
                    <div class="col-md-12">
                      <!-- for vehicle name -->
                       <div class="form-group">
                           <label for="vecreq">VEHICLE REG:</label>
                           <input type="text" class="form-control" id="regnum" onkeyup="showHint(this.value)" name="vecreq" placeholder="enter existing reg number" required>
                          
                       </div>
                    </div>
                  </div> 
                  <!-- for amount spent on the servicing -->
                  <label for="foramount">SERVICING AMOUNT:</label>
                  <div class="form-group input-group">
                    <span class="input-group-addon">₵</span>
                    <input type="text" class="form-control" name="vecsrvamnt">
                    <span class="input-group-addon">.00</span>
                  </div>
                <!-- for servicing invoice -->
                <div class="form-group">
                    <label for="vecsrvinv">SERVICING INVOICE:</label>
                    <input type="text" class="form-control" id="vecsrvinv" placeholder="Enter invoice number" name="vecsrvinv" >
                </div>
                 <!-- for servicing notes -->
                <div class="form-group">
                    <label for="vecsrvnote">SERVICING NOTES:</label>
                    <textarea type="text" class="form-control" placeholder="enter servicing notes" id="vecsrvnote" name="vecsrvnote"></textarea>
                </div>
                  </div>
                  <div class="col-md-6">
                     <!-- for company -->
                <div class="form-group">
                    <label for="vecsrvcom">SERVICING DEALERSHIP:</label>
                    <input type="text" class="form-control" id="vecsrvcom" placeholder="Enter servicing companyr" name="vecsrvcom" >
                </div>
                    <!-- for servicing date -->
                <div class="form-group">
                    <label for="vecsrvdate">SERVICING DATE:</label>
                    <input type="text" class="form-control tcal" id="vecsrvdate" name="vecsrvdate">
                </div>
                <!-- for milage -->
                <div class="form-group">
                    <label for="vecsrvmilage">SERVICING MILAGE:</label>
                    <input type="text" class="form-control" id="vecsrvmilage" name="vecsrvmilage">
                </div>
                <!-- for next servicing -->
                <div class="form-group">
                    <label for="vecsrvnxtdate">NEXT SERVICING DATE:</label>
                    <input type="text" class="form-control tcal" id="vecsrvnxtdate" name="vecsrvnxtdate">
                </div>
                  <div class="well modal-footer" id="bg">
                    <span id="txtHint"></span>  
           </div>
                  </div> 
                </div>
            <!-- sending logedin userid -->
            <input name="user_id" value="<?php echo $_SESSION['loginid'] ?>" hidden>
            <!-- sending the unique id -->
            <input name="vecsrvid" value="<?php echo  $vecsrvid; ?>" hidden>  
            <!-- --> 
        </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- for teh updat modal -->
<div class="modal fade" id="updateModal" tabindex="-1" role="dialog" aria-labelledby="updateModalLabel" aria-hidden="true">
  <div class="modal-dialog ">
    <div class="modal-content">
    </div>
  </div>
</div>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
     <script src="cdn.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="bootstrap/js/bootstrap.min.js"></script>

  </body>
</html>
