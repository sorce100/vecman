 <?php   
require('db/connection.php');
include("check.php"); 

$input=$_GET["data"];
$sql="SELECT * FROM userscomplaint WHERE uscomid='$input'";
  $result=pg_query($db,$sql);
  $row=pg_fetch_array($result);
  if (!$result) {
     header("Location:userscomplaint.php");
  }else{
    $uscomid=trim(strtoupper($row['uscomid']));
    $comname=trim(strtoupper($row['comname']));
    $vecreg=trim(strtoupper($row['vecreg'])); 
    $comcategory=trim(strtoupper($row['comcategory']));  
    $comdetails=trim(strtoupper($row['comdetails'])); 
    $comdate=trim(strtoupper($row['comdate']));  
    $comphnum=trim(strtoupper($row['comphnum'])); 
    $comstatus=trim(strtoupper($row['comstatus']));
     $comremarks=trim(strtoupper($row['comremarks']));   
  }
 ?>
 <script>
  function myFunction() {
    window.location.reload();
                      }
</script>

<div class="modal-header" id="bg">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" onClick="myFunction()"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">UPDATE USER COMPLAINT:</h4>
      </div>
      <div class="modal-body" id="bg">
      <form method="POST" action="userscomplaint_updateScript.php"> 
        <!-- for complaints name -->
                <div class="form-group">
                  <label for="comname">COMPLAINT NAME:</label>
                  <input type="text" class="form-control" id="comname"  value="<?php echo $comname;?>" name="comname" required>
              </div>
              <div class="row">
                <div class="col-md-6">
              <!-- for vehicle registration number -->
               <div class="form-group">
                  <label for="vecreg">REGISTRATION NUMBER:</label>
                  <input type="text" class="form-control" id="vecreg"  value="<?php echo $vecreg;?>" name="vecreg">
              </div>
            </div>
              <div class="col-md-6">
                <!-- for category -->
                <div class="form-group">
                    <label for="comcategory">COMPLAINT CATEGORY:</label>
                    <select name="comcategory" class="form-control">
                              <option value="<?php echo $comcategory;?>"><?php echo $comcategory;?></option>
                               <option value="servicing">Servicing</option>
                               <option value="insurance">Insurance</option>
                               <option value="roadworthy">Road Worthy</option>
                               <option value="allocation">Allocation</option>
                                
                           </select>
                </div>
              </div>
            </div>
                <!-- complaint details -->
                <div class="form-group">
                    <label for="comdetails" style="color: black;">COMPLAINT DETAILS </label>
                    <textarea class="form-control" rows="4" name="comdetails"><?php echo $comdetails;?></textarea>
                </div>
                 <!-- for remarks -->
                <div class="form-group">
                    <label for="comremarks" style="color: black;">REMARKS </label>
                    <textarea class="form-control" rows="4" name="comremarks"><?php echo $comremarks;?></textarea>
                </div>
           <!-- date for complaint -->
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                      <label for="comdate">DATE OF COMPLAINT:</label>
                      <input type="text" class="form-control tcal" id="comdate"  value="<?php echo $comdate;?>" name="comdate">
                  </div>
                  <!-- for the status of complaint -->
                  <div class="form-group">
                      <label for="comstatus">COMPLAINT STATUS:</label>
                              <select name="comstatus" class="form-control">
                                <option value="<?php echo $comstatus;?>"><?php echo $comstatus;?></option>
                                  <option value="pending">PENDING</option>
                                 <option value="resolved">RESOLVED</option>
                             </select>
                  </div>
                </div>
                <!-- for second column -->
                <div class="col-md-6">
                  <div class="form-group">
                      <label for="comphnum">PHONE NUMBER:</label>
                      <input type="text" class="form-control" id="comphnum"  value="<?php echo $comphnum;?>" name="comphnum">
                  </div>
                  <!-- button -->
                   <div class="well modal-footer" id="bg">
            <input type="submit" class="btn btn-danger" name="submit" value="UPDATE COMPLAINT" />
              </div>
                </div>
              </div>
            
            <input name="user_id" value="<?php echo $_SESSION['loginid'] ?>" hidden>
            <input name="uscomid" value="<?php echo $uscomid; ?>" hidden>
           
        </form>
      </div>