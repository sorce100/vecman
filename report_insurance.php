<?php 
include("header.php");
require('db/connection.php');
  $sql="SELECT * FROM vecinsurance";

  $result=pg_query($db,$sql);
  $count = pg_num_rows($result);
 ?>
 <!-- link for jquery scripts -->
 <!-- include jquery -->
    <script src="bootstrap/js/jquery.js"></script>
    <script src="bootstrap/js/jqueryui.js"></script>
 <script language="javascript">
  // ajax query for sending data for the search
function showHint(str) {
    if (str.length == 0) { 
       window.location.replace('report_insurance.php');
    } else {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("txtHint").innerHTML = this.responseText;               
                            }
        };
        xmlhttp.open("GET", "report_insurance_search.php?data=" + str, true);
        xmlhttp.send();
    }
}
</script>

 <h2 style="text-align: center;"><u>VEHICLES INSURANCE REPORT</u></h2>
 <!-- for showing of number or rows returned -->
 <div class="row">
        <div class="col-md-2">
          <u><h5 style="font-weight: bold;"><?php echo " Results found: ".$count; ?></h5></u>
        </div>
      </div>
 <div class="row">
    <!-- for search  -->
    <div class="col-md-10">
      <form action="#">
           <div class="form-group input-group">
              <input type="text" class="form-control" name="data" placeholder="searh here &hellip;" autocomplete="off" onkeyup="showHint(this.value)" autofocus="autofocus">
               <span class="input-group-btn"><button class="btn btn-default" disabled><i class="fa fa-search"></i></button></span>
            </div>
           </form>
    </div>
      <div class="col-md-2">
        <a href="javascript:Clickheretoprint()" class="btn btn-primary">PRINT</a>
        </div>
      </div>
      <div class="content" id="content">
        <div class="col-md-12">
         <h3 style="text-align: center;"><u>Insurance Report Details</u></h3>
        </div>
      <table class="table table-responsive table-condensed table-striped table-hover table-bordered" style="text-align: center;">
                <thead style="color: black;" class="main-color-bg"> 
                        <tr>  
                          <th style="border-bottom:1px solid #999999" width="15%">REG NUMBER</th>
                          <th style="border-bottom:1px solid #999999">NUMBER</th>
                          <th style="border-bottom:1px solid #999999">COVERAGE</th>
                          <th style="border-bottom:1px solid #999999">S/NUMBER</th>
                          <th style="border-bottom:1px solid #999999">INSURER</th>
                          <th style="border-bottom:1px solid #999999">NET PREMIUM</th>
                          <th style="border-bottom:1px solid #999999">POLICY NUMBER</th>
                          <th style="border-bottom:1px solid #999999">DUE DATE</th>
                        </tr> 
                </thead> 
                <tbody id="txtHint">
               <?php
                while($row=pg_fetch_array($result)){  //while look to fetch the result and store in a array $row.
                    $insnum=trim(strtoupper($row['insnum']));  
                    $insduedate=trim(strtoupper($row['insduedate'])); 
                    $inscomname=trim(strtoupper($row['inscomname'])); 
                    $snum=trim(strtoupper($row['snum']));
                    $inscoverage=trim(strtoupper($row['inscoverage'])); 
                    $insnetpremium=trim(strtoupper($row['insnetpremium'])); 
                    $inspolicy=trim(strtoupper($row['inspolicy'])); 
                    $vecregnum=trim(strtoupper($row['vecregnum'])); 
                    ?> 
                     <tr> 
                        <td style="border-bottom:1px solid #999999"><?php echo $vecregnum;?></td>
                        <td style="border-bottom:1px solid #999999"><?php echo $insnum;?></td>
                        <td style="border-bottom:1px solid #999999"><?php echo $inscoverage;?></td>
                        <td style="border-bottom:1px solid #999999"><?php echo $snum;?></td>
                        <td style="border-bottom:1px solid #999999"><?php echo $inscomname;?></td>
                        <td style="border-bottom:1px solid #999999"><?php echo $insnetpremium;?></td>
                        <td style="border-bottom:1px solid #999999"><?php echo $inspolicy;?></td>
                        <td style="border-bottom:1px solid #999999"><?php echo $insduedate;?></td>
                      </tr>  
                       
                    <?php } ?> 
                  </tbody>
            </table>
          </div>
 <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
     <script src="cdn.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="bootstrap/js/bootstrap.min.js"></script>

  </body>
</html>

