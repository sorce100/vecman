 <?php   
require('db/connection.php');
$input=$_GET["data"];
$sql="SELECT * FROM driverlicence WHERE driverlinid='$input'";
  $result=pg_query($db,$sql);
  $row=pg_fetch_array($result);
  if (!$result) {
     header("Location:vecInsurance.php");
  }else{
    $driverlinid=trim(strtoupper($row['driverlinid']));
    $fname=trim(strtoupper($row['driverfirstname']));
    $lname=trim(strtoupper($row['driverlastname']));  
    $dob=trim(strtoupper($row['driverdob']));  
    $nationality=trim(strtoupper($row['drivernationality'])); 
    $processcen=trim(strtoupper($row['driverprocesscen']));  
    $linclass=trim(strtoupper($row['driverlinclass']));
    $cetcom=trim(strtoupper($row['drivercertcom']));
    $expiry=trim(strtoupper($row['driverlinexp']));  
    $certdate=trim(strtoupper($row['drivercertdate'])); 
     $lindateissue=trim(strtoupper($row['driverlindateissue']));
     $firstlin=trim(strtoupper($row['driverfirstlin'])); 
  }
 ?>
 <script>
  function myFunction() {
    window.location.reload();
                      }
</script>
<div class="modal-header" id="bg">
        <button type="button" class="close" onClick="myFunction()" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">UPDATE LICENCE</h4>
      </div>
<div class="modal-body" id="bg">
        <form method="POST" action="vecDriversLin_UpdateScript.php"> 
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                <label for="driverfname">FIRST NAME:</label>
                <input type="text" class="form-control" id="driverfname" value="<?php echo $fname;?>" name="driverfname">
            </div>
            <div class="form-group">
                <label for="driverlname ">LAST NAME:</label>
                <input type="text" class="form-control" id="driverlname" value="<?php echo $lname;?>" name="driverlname" >
            </div>
            <!-- for drivers date of birth -->
            <div class="form-group">
                <label for="driverdob">DATE OF BIRTH:</label>
                <input type="text" class="form-control" id="driverdob" value="<?php echo $dob;?>" name="driverdob">
            </div>
            <!-- for processing center -->
             <div class="form-group">
                <label for="driverprocen">PROCESSING CENTER:</label>
                <input type="text" class="form-control" id="driverprocen" value="<?php echo $processcen;?>" name="driverprocen">
            </div>
            <!-- for nationality -->
             <div class="form-group">
                <label for="drivernation">NATIONALITY:</label>
                <input type="text" class="form-control" id="drivernation" value="<?php echo $nationality;?>" name="drivernation">
            </div>
            <!-- for class of licence -->
            <div class="form-group">
                <label for="driverlinclass">LICENCE CLASS:</label>
                <input type="text" class="form-control" id="driverlinclass" value="<?php echo $linclass;?>" name="driverlinclass">
            </div>
              </div>
              <div class="col-md-6">
                 <!-- for date of issue -->
            <div class="form-group">
                <label for="driverlindteissue">DATE OF ISSUE:</label>
                <input type="text" class="form-control" id="driverlindteissue" value="<?php echo $lindateissue;?>" name="driverlindteissue">
            </div>
            <!-- for expiry date -->
            <div class="form-group">
                <label for="driverlinexp">EXPIRY DATE:</label>
                <input type="text" class="form-control" id="driverlinexp" value="<?php echo $expiry;?>" name="driverlinexp">
            </div>
            <!-- for date of first licence -->
            <div class="form-group">
                <label for="driverfrstlin">DATE OF FIRST LICENCE:</label>
                <input type="text" class="form-control" id="driverfrstlin" value="<?php echo $firstlin;?>" name="driverfrstlin">
            </div>
            <!-- for certificate date -->
             <div class="form-group">
                <label for="drivercertdte">CERTIFICATE DATE:</label>
                <input type="text" class="form-control" id="drivercertdte" value="<?php echo $certdate;?>" name="drivercertdte">
            </div>
            <!-- for certificate of competence -->
            <div class="form-group">
                <label for="drivercertcom">CERTIFICATE OF COMPETENCE:</label>
                <input type="text" class="form-control" id="drivercertcom" value="<?php echo $cetcom;?>" name="drivercertcom">
            </div>
            <input name="driverlinid" value="<?php echo $driverlinid; ?>" hidden>
            <div class="well modal-footer" id="bg">
            <input type="submit" class="btn btn-danger" name="submit" value="UPDATE LICENCE" />
              </div>
            </div>
            
        </div>
        </form>
      </div>