<?php

require('fpdf/fpdf.php');
$pdf = new FPDF('P','mm','A4');
$pdf->AddPage();

// width,ss
$pdf->Image("images/printlogo.jpg",70,10,60,60);
// setting fonts using timees new rman
$pdf->SetFont('Times','B',12);
// line break
$pdf->Ln(60);
// for the page title
$pdf->SetFont('Times','BU',20);
$pdf->Cell(190,10,'MINISTRY OF LANDS AND NATURAL RESOURCES',0,1,'C');
$pdf->Cell(190,10,'VEHICLE USER COMPLAINT FORM',0,1,'C');
$pdf->Ln(5);
// for vechicle details
// setting leftmargin
// $pdf->SetLeftMargin(30);
// $pdf->Cell(60,10,"RoadWorthy details:",0,1);
$pdf->SetLeftMargin(25);
// for the body
$pdf->SetFont('Times','B',15);
// for name
$pdf->Cell(20,10,'FULL NAME:',0,1);
$pdf->Cell(160,10,"",1,1);
$pdf->Ln(2);
// for title
$pdf->Cell(20,10,'TITLE:',0,1);
$pdf->Cell(160,10,"",1,1);
$pdf->Ln(2);
// for registration number
$pdf->Cell(20,10,'REGISTRATION NUMBER:',0,1);
$pdf->Cell(160,10,"",1,1);
$pdf->Ln(2);
// for details
$pdf->Cell(20,10,'PROBLEM:',0,1);
$pdf->Cell(160,40,"",1,1);
$pdf->Ln(2);
// for phone number
$pdf->Cell(20,10,'PHONE NUMBER:',0,1);
$pdf->Cell(160,10,"",1,1);
$pdf->Ln(2);
// for date
$pdf->Cell(20,10,'DATE:',0,1);
$pdf->Cell(70,10,"",1,0);
$pdf->Ln(2);


$pdf->Output();
?>