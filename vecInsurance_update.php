 <?php   
require('db/connection.php');
include("check.php"); 

$input=$_GET["data"];
$sql="SELECT * FROM vecinsurance WHERE vecinsid='$input'";
  $result=pg_query($db,$sql);
  $row=pg_fetch_array($result);
  if (!$result) {
     header("Location:vecInsurance.php");
  }else{
    $vecinsid = trim(strtoupper($row['vecinsid'])); 
    $vecid=trim(strtoupper($row['vecid'])); 
    $insnum=trim(strtoupper($row['insnum']));  
    $insduedate=trim(strtoupper($row['insduedate'])); 
    $inscomname=trim(strtoupper($row['inscomname'])); 
    $snum=trim(strtoupper($row['snum']));
    $inscoverage=trim(strtoupper($row['inscoverage'])); 
    $insnetpremium=trim(strtoupper($row['insnetpremium'])); 
    $inspolicy=trim(strtoupper($row['inspolicy'])); 
    $vecregnum=trim(strtoupper($row['vecregnum']));  
  }
 ?>
 <script>
  function myFunction() {
    window.location.reload();
                      }
</script>

<div class="modal-header" id="bg">
        <button type="button" class="close" onClick="myFunction()" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">UPDATE INSURANCE</h4>
      </div>
<div class="modal-body" id="bg">
        <form method="POST" action="vecInsurance_UpdateScript.php"> 
                <div class="row">
                <div class="col-md-6">
                  <!-- for vehicle name -->
                <div class="form-group">
                    <label for="regnum">VEHICLE REG:</label>
                    <input type="text" class="form-control" id="regnum" value="<?php echo $vecregnum;?>" name="regnum" disabled >
                </div>
                <!-- insurance number -->
                <div class="form-group">
                    <label for="insnum">INSURANCE NUMBER:</label>
                    <input type="text" class="form-control" id="insnum" value="<?php echo $insnum;?>" name="insnum">
                </div>
                <!-- for insurance coverage -->
                <div class="form-group">
                    <label for="inscov">INSURANCE COVERAGE:</label>
                    <input type="text" class="form-control" id="inscov" value="<?php echo $inscoverage;?>" name="inscov">
                </div>
                <!-- S/NUMBER  -->
                <div class="form-group">
                    <label for="snum">S/NO:</label>
                    <input type="text" class="form-control" id="snum" value="<?php echo $snum;?>" name="snum">
                </div>
                 <div  id="bg">
                    <a href="vecInsurance_report.php?data=<?php echo $vecregnum;?>" class="btn btn-md btn-default">PRINT<span class="glyphicon glyphicon-print"></span></a>
                 </div>
                </div>  
                <div class="col-md-6">
                  <!-- insurance company name -->
                <div class="form-group">
                    <label for="inscomname">INSURANCE COMPANY:</label>
                    <input type="text" class="form-control" id="inscomname" value="<?php echo $inscomname;?>" name="inscomname">
                </div>
                <!-- insurance net premium -->
                <div class="form-group">
                    <label for="insnetpre">NET PREMIUM:</label>
                    <input type="text" class="form-control" id="insnetpre" value="<?php echo $insnetpremium;?>" name="insnetpre">
                </div>
                <!-- for insurance policy -->
                <div class="form-group">
                    <label for="inspolicy">POLICY NUMBER:</label>
                    <input type="text" class="form-control" id="inspolicy" value="<?php echo $inspolicy;?>" name="inspolicy">
                </div>
                
                <!-- insurance due dat -->
                <div class="form-group">
                    <label for="insduedate">DUE DATE:</label>
                    <input type="text" class="form-control" id="insduedate" value="<?php echo $insduedate;?>" name="insduedate" requierd>
                </div>
            <!-- sending vechicle insurance id -->
               <input name="vecinsid" value="<?php echo $vecinsid;?>" hidden>
              <div class="well modal-footer" id="bg">
              <input type="submit" class="btn btn-md btn-danger" name="submit" value="UPDATE INSURANCE" />
               <!-- sending logedin userid -->
            <input name="user_id" value="<?php echo $_SESSION['loginid'] ?>" hidden>
            <input name="vecreg" value="<?php echo  $vecregnum ?>" hidden>

           </div>
           </div>
           </div>
        </form>
      </div>